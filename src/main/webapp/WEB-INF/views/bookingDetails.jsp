<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">
<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-content table-responsive">
									<c:choose>
										<c:when test="${booking != null}">
											<table class="table">
												<tbody>
													<tr>
														<td>Ticket ID</td>
														<td>${booking.getTicketId()}</td>
													</tr>
													<tr>
														<td>From - To</td>
														<td>${booking.getBusRoute().getRoute().getFrom().getLocation()}
															-
															${booking.getBusRoute().getRoute().getTo().getLocation()}</td>
													</tr>
													<tr>
														<td>Departure - Arrival Time</td>
														<td>${booking.getBusRoute().getDepartureTime()}-
															${booking.getBusRoute().getArrivalTime()}</td>
													</tr>
													<tr>
														<td>Seat Details</td>
														<td><c:forEach items="${booking.getBookingDetails()}"
																var="bookingDetail">
																<c:if
																	test="${bookingDetail.getStatus().getStatus() != 'cancelled'}">
																	<table>
																		<tr>
																			<td>Name</td>
																			<td>${bookingDetail.getPassengerName()}</td>
																		</tr>
																		<tr>
																			<td>Seat No.</td>
																			<td>${bookingDetail.getSeat().getSeatNo()}</td>
																		</tr>
																	</table>
																</c:if>
															</c:forEach></td>

													</tr>
													<tr>
														<td>User Name</td>
														<td>${booking.getUser().getFirstName()}</td>
													</tr>
													<tr>
														<td>User E-mail</td>
														<td>${booking.getUser().getEmailId()}</td>
													</tr>
													<tr>
														<td>Bus reg.no</td>
														<td>${booking.getBusRoute().getBus().getRegNo()}</td>
													</tr>
												</tbody>
											</table>
											<button type="button" class="btn btn-warning btn-md"
												data-toggle="modal" data-target="#myModal">Cancel
												Ticket</button>
											<div class="clearfix"></div>
											<c:if test="${success != null}">
												<div class="row">
													<div class="col-md-12 text-success">
														<div class="panel-body">${success}</div>
													</div>
												</div>
											</c:if>
										</c:when>
										<c:otherwise>
											<div class="row">
												<div class="col-md-12 text-danger">
													<div class="panel-body">Ticket Id expired</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class="row"></div>
				<form action="ticketCancel">
					<div class="row">
						<div class="col-md-6 col-md-push-1">
							<div class="form-group label-floating">
								<label class="control-label">Seat no.</label> <select
									name="passengerId" title="Select seat number"
									class="form-control" title="Select seat number">
									<c:forEach items="${booking.getBookingDetails()}"
										var="bookingDetail">
										<option value="${bookingDetail.getSeat().getSeatNo()}">
											${bookingDetail.getSeat().getSeatNo()}</option>
									</c:forEach>
								</select>
							</div>
							<input type="hidden" name="emailId" value="${sessionScope.id}">
							<input type="hidden" name="ticketId"
								value="${booking.getTicketId()}">
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-danger pull-right">Cancel
							Ticket</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
