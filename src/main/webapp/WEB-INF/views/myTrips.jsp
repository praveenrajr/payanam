<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<!--	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />-->
<!--	<link rel="icon" type="image/png" href="../assets/img/favicon.png" />-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
<body>
	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h3>Upcoming Trips</h3>
								</div>
								<div class="card-content table-responsive">
									<c:if test="${success != null}">
										<div class="row">
											<div class="col-md-12 text-success">
												<div class="panel-body">
													<h4>${success}</h4>
												</div>
											</div>
										</div>
									</c:if>
									<c:choose>
										<c:when test="${not empty upcomingTrips}">

											<c:forEach items="${upcomingTrips}" var="upcomingTrip">
												<table class="table">
													<thead class="text-accent">
														<th>Trip Date</th>
														<th>From - To</th>
														<th>Departure time - Arrival time</th>
														<th>TicketId</th>
													</thead>
													<tbody>
														<tr>
															<td>${upcomingTrip.getBusRoute().getTripDate()}</td>
															<td>${upcomingTrip.getBusRoute().getRoute().getFrom().getLocation()}
																-
																${upcomingTrip.getBusRoute().getRoute().getTo().getLocation()}</td>
															<td>${upcomingTrip.getBusRoute().getDepartureTime()}-
																${upcomingTrip.getBusRoute().getArrivalTime()}</td>
															<td><a
																href="getBookedDetail?ticketId=${upcomingTrip.getTicketId()}&emailId=${sessionScope.id}">${upcomingTrip.getTicketId()}</a></td>
														</tr>
													</tbody>
												</table>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<div class="row">
												<div class="col-md-12 text-danger">
													<div class="panel-body">
														No trips available, You can start your journey<a
															href="home"> Here</a>
													</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-header">
									<h3>Past Trips</h3>
								</div>
								<div class="card-content table-responsive">
									<c:choose>
										<c:when test="${not empty pastTrips}">

											<c:forEach items="${pastTrips}" var="pastTrip">
												<table class="table">
													<thead class="text-primary">
														<th>Trip Date</th>
														<th>From - To</th>
														<th>Departure time - Arrival time</th>
														<th>TicketId</th>
													</thead>
													<tbody>
														<tr>
															<td>${pastTrip.getBusRoute().getTripDate()}</td>
															<td>${pastTrip.getBusRoute().getRoute().getFrom().getLocation()}
																-
																${pastTrip.getBusRoute().getRoute().getTo().getLocation()}</td>
															<td>${pastTrip.getBusRoute().getDepartureTime()}-
																${pastTrip.getBusRoute().getArrivalTime()}</td>
														</tr>
													</tbody>
												</table>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<div class="row">
												<div class="col-md-12 text-danger">
													<div class="panel-body">You don't have any past
														records.</div>
												</div>
											</div>
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</body>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>