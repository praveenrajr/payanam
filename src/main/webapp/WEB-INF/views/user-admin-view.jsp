<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>

	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<c:if test="${user != null}">
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-10">
								<div class="card col-md-4">
									<div class="card-header col-md-6" data-background-color="grey">
										<h4 class="title">Edit Profile</h4>
									</div>
									<div class="card-content">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Firstname</label>
													<div>${user.getFirstName()}</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Lastname</label>

													<div>${user.getLastName()}</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">DOB</label>
													<div>${user.getDOB()}</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Phone Number</label>
													<div>${user.getPhoneNo()}</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">E-Mail</label>
													<div>${user.getEmailId()}</div>
												</div>
											</div>
										</div>
										<c:if test="${success != null}">
											<div class="row">
												<div class="col-md-12 text-success">
													<span>${success}</span>
												</div>
											</div>
										</c:if>
										<c:if test="${error != null}">
											<div class="row">
												<div class="col-md-12 text-danger">
													<span>${error}</span>
												</div>
											</div>
										</c:if>
									</div>
								</div>
							</div>
						</div>


						<div class="row">
							<c:if test="${success != null}">

								<div class="col-md-12">
									<div class="card">
										<div class="card-header">
											<h3>Upcoming Trips</h3>
										</div>
										<div class="card-content table-responsive">
											<div class="row">
												<div class="col-md-12 text-success">
													<div class="panel-body">
														<h4>${success}</h4>
													</div>
												</div>
											</div>
							</c:if>
							<c:choose>
								<c:when test="${not empty upcomingTrips}">

									<c:forEach items="${upcomingTrips}" var="upcomingTrip">
										<table class="table">
											<thead class="text-accent">
												<th>Trip Date</th>
												<th>From - To</th>
												<th>Departure time - Arrival time</th>
												<th>TicketId</th>
											</thead>
											<tbody>
												<tr>
													<td>${upcomingTrip.getBusRoute().getTripDate()}</td>
													<td>${upcomingTrip.getBusRoute().getRoute().getFrom().getLocation()}
														-
														${upcomingTrip.getBusRoute().getRoute().getTo().getLocation()}</td>
													<td>${upcomingTrip.getBusRoute().getDepartureTime()}-
														${upcomingTrip.getBusRoute().getArrivalTime()}</td>
													<td><a
														href="getBookedDetail?ticketId=${upcomingTrip.getTicketId()}&emailId=${sessionScope.id}">${upcomingTrip.getTicketId()}</a></td>
												</tr>
											</tbody>
										</table>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<div class="row">
										<div class="col-md-12 text-danger">
											<div class="panel-body">
												<c:choose>
													<c:when test="${user.gender == 'male'}">
												Mr.
											</c:when>
													<c:otherwise>
												Ms/Mrs
											</c:otherwise>
												</c:choose>
												${user.getFirstName()} has not yet booked a ticket with us !
												&#128532;
											</div>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
		</div>
		<c:choose>
			<c:when test="${not empty pastTrips}">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h3>Past Trips</h3>
							</div>
							<div class="card-content table-responsive">
								<c:forEach items="${pastTrips}" var="pastTrip">
									<table class="table">
										<thead class="text-primary">
											<th>Trip Date</th>
											<th>From - To</th>
											<th>Departure time - Arrival time</th>
											<th>TicketId</th>
										</thead>
										<tbody>
											<tr>
												<td>${pastTrip.getBusRoute().getTripDate()}</td>
												<td>${pastTrip.getBusRoute().getRoute().getFrom().getLocation()}
													-
													${pastTrip.getBusRoute().getRoute().getTo().getLocation()}</td>
												<td>${pastTrip.getBusRoute().getDepartureTime()}-
													${pastTrip.getBusRoute().getArrivalTime()}</td>
											</tr>
										</tbody>
									</table>
								</c:forEach>
			</c:when>
		</c:choose>
	</div>
	</div>
	</div>
	</div>

	</div>
	</div>
	</c:if>
	<%@ include file="footer.jsp"%>
	</div>
	</div>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
