<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>

	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<c:if test="${user != null}">
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-10">
								<div class="card col-md-4">
									<div class="card-header col-md-6" data-background-color="grey">
										<h4 class="title">Edit Profile</h4>
									</div>
									<div class="card-content">
										<form id="formValidate" method="get" action="updateUser">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group label-floating">
														<label class="control-label">Fist Name</label> <input
															type="text" name="firstName" pattern="[a-zA-Z]+"
															class="form-control" value="${user.getFirstName()}">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group label-floating">
														<label class="control-label">Last Name</label> <input
															type="text" name="lastName" pattern="[a-zA-Z]+"
															class="form-control" value="${user.getLastName()}">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group label-floating">
														<label class="control-label">DOB</label> <input
															type="text" name="DOB" class="form-control datepicker"
															value="${user.getDOB()}">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group label-floating">
														<label class="control-label">Phone Number</label> <input
															type="text" name="phoneNo" pattern="[0-9]{10}"
															maxlength="10" class="form-control"
															value="${user.getPhoneNo()}">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<div class="form-group label-floating">
														<label class="control-label">E-Mail</label> <input
															type="text" name="emailId" class="form-control"
															value="${user.getEmailId()}" readonly>
													</div>
												</div>
											</div>
											<c:if test="${success != null}">
												<div class="row">
													<div class="col-md-12 text-success">
														<span>${success}</span>
													</div>
												</div>
											</c:if>
											<c:if test="${error != null}">
												<div class="row">
													<div class="col-md-12 text-danger">
														<span>${error}</span>
													</div>
												</div>
											</c:if>
											<button type="submit" class="btn btn-primary pull-right"
												data-background-color="green">Update Profile</button>
											<div class="clearfix"></div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			<%@ include file="footer.jsp"%>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
