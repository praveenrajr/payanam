<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<%@ include file="header.jsp"%>
		<div class="main-panel">
			<div class="content">
				<div class="row">
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-12">
								<div class="card">
									<div class="card-header" data-background-color="purple">
										<h4 class="title">Users</h4>
									</div>
									<div class="card-content table-responsive">
										<table id="table" class="table table-hover">
											<thead class="text-primary">
												<th>Id</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone no</th>
												<th>age</th>
												<th></th>
											</thead>
											<tbody>
												<c:forEach items="${users}" var="user" varStatus="count">
													<tr>
														<td>${user.id}</td>
														<td>${user.firstName}</td>
														<td>${user.emailId}</td>
														<td>${user.phoneNo}</td>
														<td>${user.age}</td>
														<td ${user.role.roleType}></td>
														<td>
															<div>
																<a href="getUserAndTrip?emailId=${user.emailId}"> <i
																	class="material-icons">visibility</i>
																</a>
															</div>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%@ include file="footer.jsp"%>
			</div>
		</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>
<script src="resources/js/table.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>