<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<link href="resources/css/timepicker.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">


<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<c:if test="${message != null}">
		<script>
			alert("${message}");
			window.location.assign("/getAllBuses");
		</script>
	</c:if>
	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10">
							<div class="card col-md-4">
								<div class="card-header col-md-6" data-background-color="grey">
									<h4 class="title">Bus</h4>
								</div>
								<div class="card-content">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group label-floating">
												<label class="control-label">Bus name / Travels name</label>
												${bus.name}"
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group label-floating">
												<label class="control-label">Registration no</label>
												${bus.regNo}
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">Total seats</label>
												${bus.totalSeats}
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">Phone Number</label>
												${bus.busType.busType}
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">Status</label>
												${bus.status.status}
											</div>
										</div>
									</div>
									<c:if test="${bus.status.status != 'Deleted' }">



										<div class="row">
											<div class="col-md-6">
												<a href="deleteBus?regNo=${bus.regNo}"
													onclick="return confirm('Are you sure you want to delete bus')">
													<i class="material-icons">delete</i>
												</a>
											</div>

											<div class="col-md-6">
												<a> <i class="material-icons" data-toggle="modal"
													data-target="#myModal">add_circle</i>
												</a>
											</div>
										</div>
									</c:if>
									<c:if test="${null == bus.busRoutes}">
										<table id="table" class="table table-hover">
											<thead class="text-primary">
												<th>From</th>
												<th>To</th>
												<th>Date</th>
												<th>Available seats</th>
											</thead>
											<tbody>
												<c:forEach items="${bus.busRoutes}" var="busRoute"
													varStatus="count">
													<tr>
														<td>${busRoute.route.from.location}</td>
														<td>${busRoute.route.to.location}</td>
														<td>${busRoute.tripDate}</td>
														<td>${busRoute.arrivalTime}</td>
														<td>${busRoute.departureTime}</td>
														<td>${busRoute.availableSeats}</td>

													</tr>
												</c:forEach>
											</tbody>
										</table>
									</c:if>
									<c:if test="${success != null}">
										<div class="row">
											<div class="col-md-12 text-success">
												<span>${success}</span>
											</div>
										</div>
									</c:if>
									<c:if test="${error != null}">
										<div class="row">
											<div class="col-md-12 text-danger">
												<span>${error}</span>
											</div>
										</div>
									</c:if>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="footer.jsp"%>
		</div>
	</div>
	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Route</h4>
					<h5 class="modeal-category">${bus.name}(${bus.regNo})</h5>
				</div>
				<div class="modal-body">
					<form Id="seatchFormValidate" method="post" action=addTrip>
						<div class="row">
							<div class="col-md-6">
								<input type="hidden" name="regNo" value="${bus.regNo}">
								<div class="form-group label-floating">
									<label class="control-label">From</label> <select name="from"
										title="Select start location" class="form-control"
										title="Select start location">
										<c:forEach items="${locations}" var="location"
											varStatus="count">
											<option value="${location.getId()}">
												${location.getLocation()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">To</label> <select name="to"
										title="Select destination place" class="form-control"
										title="Select end location">
										<c:forEach items="${locations}" var="location"
											varStatus="count">
											<option value="${location.getId()}">
												${location.getLocation()}</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">TripDate</label> <input
										type="text" name="tripDate" class="form-control datepicker"
										value="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group label-floating">
									<label class="control-label">Price</label> <input type="number"
										name="price" class="form-control" value="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="input-group bootstrap-timepicker timepicker">
									<input id="timepicker1" type="text"
										class="form-control input-small" name="arrivalTime"> <span
										class="input-group-addon"><i
										class="glyphicon glyphicon-time"></i></span>
								</div>


							</div>
							<div class="col-md-6">
								<div class="input-group bootstrap-timepicker timepicker">
									<input id="timepicker2" type="text"
										class="form-control input-small" name="departureTime">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-time"></i></span>
								</div>


							</div>
						</div>
						<c:if test="${message != null}">
							<div class="row">
								<div class="col-md-12 text-danger">
									<span>${error}</span>
								</div>
							</div>
						</c:if>
						<c:if test="${error != null}">
							<div class="row">
								<div class="col-md-12 text-danger">
									<span>${error}</span>
								</div>
							</div>
						</c:if>
						<button type="submit" class="btn btn-primary pull-left"
							data-background-color="accent">Add</button>
						<div class="clearfix"></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->


<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/search.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript"
	src="resources/js/bootstrap-timepicker.js"></script>

<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
<script type="text/javascript">
	$('#timepicker1').timepicker();
	$('#timepicker2').timepicker();
</script>
</html>
