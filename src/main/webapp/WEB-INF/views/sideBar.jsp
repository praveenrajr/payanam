<div class="sidebar" data-color="purple">
	<div class="logo">
		<a href="home" class="simple-text"> Payanam </a>
	</div>

	<div class="sidebar-wrapper">
		<ul class="nav">
			<li><a href="home"> <i class="material-icons"></i>
					<p>Book Ticket</p>
			</a></li>
			<li><a href="myTrips"><i class="material-icons"></i>
					<p>My Trips</p> </a></li>
			<li><a href="getUser"> <i class="material-icons"></i>
					<p>My Profile</p>
			</a></li>
			<li><a href="updatePassword"> <i
					class="material-icons text-gray"></i>
					<p>Change Password</p>
			</a></li>
			<c:if test="${sessionScope.role == 'admin'}">
				<li><a href="getBuses"> <i class="material-icons text-gray"></i>
						<p>Bus</p>
				</a></li>
				<li><a href="getAllUser"> <i
						class="material-icons text-gray"></i>
						<p>User</p>
				</a></li>
			</c:if>
		</ul>
	</div>
</div>
