<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<!--	<link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />-->
<!--	<link rel="icon" type="image/png" href="../assets/img/favicon.png" />-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">
<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>
	<div class="wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="card col-md-4">
							<div class="card-content">
								<form Id="formValidate" method="post" action="addUser">
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Fist Name</label> <input
													type="text" name="firstName" pattern="[a-zA-Z]+"
													class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Last Name</label> <input
													type="text" name="lastName" pattern="[a-zA-Z]+"
													class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-1">
											<div class="form-group label-floating">
												<label class="control-label">Male</label> <input
													type="radio" name="gender" class="form-control"
													value="male" checked="checked">
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group label-floating">
												<label class="control-label">Female</label> <input
													type="radio" name="gender" class="form-control"
													value="female">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">DOB</label> <input type="text"
													name="DOB" class="form-control datepicker">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Password</label> <input
													type="password" name="password" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Confirm Password</label> <input
													type="password" name="confirmPassword" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">E-Mail</label> <input
													type="email" name="emailId" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Phone Number</label> <input
													type="text" name="phoneNo" pattern="[0-9]{10}"
													class="form-control">
											</div>
										</div>
									</div>
									<c:if test="${error != null}">
										<div class="row">
											<div class="col-md-12 text-danger">
												<span>${error}</span>
											</div>
										</div>
									</c:if>
									<button type="submit" class="btn btn-primary pull-left"
										data-background-color="green">Register</button>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
