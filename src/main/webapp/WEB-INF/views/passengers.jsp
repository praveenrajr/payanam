<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">
<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>
<body>
	<div class="wrapper">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="card col-md-4">
							<div class="card-content">
								<div id="inputs"></div>
								<c:set var="i" value="1" scope="page" />
								<form Id="passengerFormValidate" method="get"
									action="addBooking?noOfPassengers=${i}">
									<c:forEach items="${SelectedSeats}" var="selectedSeat"
										varStatus="count">
										<div class="row">
											<div class="col-md-10">
												<div class="form-group label-floating">
													<label class="control-label">Passenger name</label> <input
														type="text" name="name${i}" id="name" pattern="[a-zA-Z]+"
														class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-10">
												<div class="form-group label-floating">
													<label class="control-label">Seat no</label> <input
														type="hidden" name="seat${i}" value="${selectedSeat}"
														readonly class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-1">
												<div class="form-group label-floating">
													<label class="control-label">Male</label> <input
														type="radio" name="gender${i}" id="gender"
														class="form-control" value="male" checked="checked">
												</div>
											</div>
											<div class="col-md-1">
												<div class="form-group label-floating">
													<label class="control-label">Female</label> <input
														type="radio" id="gender" name="gender${i}"
														class="form-control" value="female">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-10">
												<div class="form-group label-floating">
													<label class="control-label">Age</label> <input type="text"
														id="passengerAge" name="age${i}" maxlength="2"
														class="form-control">
												</div>
											</div>
										</div>
										<c:set var="i" value="${i + 1}" scope="page" />
									</c:forEach>

									<div>
										<h3>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">E-Mail</label> <input
													type="email" name="emailId" class="form-control"
													value="${sessionScope.id}">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-10">
											<div class="form-group label-floating">
												<label class="control-label">Phone Number</label> <input
													type="text" name="phoneNo" pattern="[0-9]{10}"
													class="form-control">
											</div>
										</div>
									</div>
									<c:if test="${error != null}">
										<div class="row">
											<div class="col-md-12 text-danger">
												<span>${error}</span>
											</div>
										</div>
									</c:if>
									<input type="hidden" name="noOfPassenger" value="${i}" /> <input
										type="hidden" name="busRouteId" value="${busRouteId}">
									<button type="submit" class="btn btn-primary pull-left"
										data-background-color="green">Book now</button>
									<div class="clearfix"></div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
