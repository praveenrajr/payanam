<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrap-datepicker3.css" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>
	<div class="wrapper">
		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-12">
							<div class="card col-md-4">
								<div class="card-content">
									<form method="get" id="searchValidate" action="search">
										<div class="row">
											<div class="col-md-1"></div>
											<div class="col-md-3">
												<div class="form-group label-floating">
													<label class="control-label">From</label> <select
														name="from" title="Select start location"
														class="form-control" title="Select start location">
														<c:forEach items="${locations}" var="location"
															varStatus="count">
															<option value="${location.getId()}">
																${location.getLocation()}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group label-floating">
													<label class="control-label">To</label> <select name="to"
														title="Select destination place" class="form-control"
														title="Select end location">
														<c:forEach items="${locations}" var="location"
															varStatus="count">
															<option value="${location.getId()}">
																${location.getLocation()}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group label-floating">
													<label class="control-label">Date</label> <input
														type="text" name="tripDate" title="Enter trip date"
														class="form-control datepicker">
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group label-floating">
													<label class="control-label"></label>
													<button type="submit" class="btn btn-primary form-control"
														data-background-color="green">Search</button>
												</div>
											</div>
										</div>
										<div class="col-md-1"></div>
										<c:if test="${message != null}">
											<div class="row">
												<div class="col-md-11 col-md-push-1 text-danger">
													${message}</div>
											</div>
										</c:if>
										<c:if test="${success != null}">
											<div class="row">
												<div class="col-md-11 col-md-push-1 text-danger">
													${success}</div>
											</div>
										</c:if>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="footer.jsp"%>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapSearchValidate.js"></script>
<script type="text/javascript" src="resources/js/datepicker.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrap-datepicker.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>