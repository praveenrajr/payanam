<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">
<link rel="stylesheet" href="resources/css/jquery.seat-charts.css">
<link rel="stylesheet" href="resources/css/style.css">

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<link href="resources/css/seat.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">

<!--   Core JS Files   -->
<script src="resources/js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>

<script type="text/javascript" src="resources/js/seat.js"></script>

<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</head>
<body>

	<div class="wrapper">

		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10">
							<div class="card">
								<div class="card-header col-md-6 cus-head"
									data-background-color="grey">
									<h4 class="title">Select Seats</h4>
								</div>
								<div class="card-content">
									<form action="passengers-form"
										onsubmit="return checkSelection()">
										<div class=row>
											<div class="col-md-6 fuselage">
												<input type="hidden" name="busRouteId"
													value="${busRoute.id}"> <input type="hidden"
													id="price" value="${busRoute.price}" />
												<c:set var="column_count" value="1" scope="page" />
												<c:if test="${null != Seats}">
													<div class="plane">
														<c:forEach items="${Seats}" var="seat" varStatus="count">
															<c:if test="${column_count == 1 }">
																<c:if test="${column_count == 1}">
																	<ol class="seats" type="A">
																</c:if>
															</c:if>
															<c:if test="${column_count == 2}">
																<br>
															</c:if>
															<c:choose>
																<c:when
																	test="${seat.value.getStatus().getStatus() == 'Booked'}">
																	<li class="seat"><input class="boxes"
																		type="checkbox" name="seats"
																		value="${seat.value.getId()}" id="${seat.key}"
																		disabled="true" /> <label for="${seat.key}"><c:out
																				value="${seat.value.getSeatNo()}"></c:out></label></li>
																</c:when>

																<c:otherwise>
																	<li class="seat"><input class="boxes"
																		type="checkbox" name="seats"
																		value="${seat.value.getId()}" id="${seat.key}"
																		data-max="3" /> <label for="${seat.key}"><c:out
																				value="${seat.value.getSeatNo()}"></c:out></label></li>
																</c:otherwise>
															</c:choose>
															<c:if test="${column_count == 4}">
																</ol>
															</c:if>
															<c:choose>
																<c:when test="${column_count < 4}">
																	<c:set var="column_count" value="${column_count + 1}"
																		scope="page" />
																</c:when>
																<c:otherwise>
																	<br>
																	<c:set var="column_count" value="${column_count = 1}"
																		scope="page" />
																</c:otherwise>
															</c:choose>
														</c:forEach>
													</div>
												</c:if>
											</div>
											<div class="col-md-4">
												<div>
													<p>
													<h4>${busRoute.bus.name}</h4>
													</p>
												</div>
												<div>
													<p>${busRoute.route.from.location}-
														${busRoute.route.to.location}</p>
												</div>
												<div class="row">
													<div class="col-md-4">
														<h6>price</h6>
													</div>
													<div class="col-md-4">
														<p>${busRoute.price}</p>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<h6>total</h6>
													</div>
													<div class="col-md-4">
														<p id="total"></p>
													</div>
												</div>
												<div class="row">
													<!-- 													<div class="col-md-10"> -->
													<!-- 														<div id="form"></div> -->
													<!-- 													</div> -->
													<div>
														<button type="submit" class="btn btn-primary pull-right"
															data-background-color="green">Book now</button>
														</a>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<%@ include file="footer.jsp"%>
			</div>
		</div>
</body>
</html>
