<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="resources/css/bootstrapValidator.min.css">

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>

	<div class="wrapper">

		<%@ include file="sideBar.jsp"%>
		<div class="main-panel">
			<%@ include file="header.jsp"%>
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10">
							<div class="card">
								<div class="card-header col-md-6" data-background-color="grey">
									<h4 class="title">Change Password</h4>
								</div>
								<div class="card-content">
									<form method="post" action="changePassword">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">E-mail</label> <input
														type="text" name="emailId" class="form-control"
														value="${sessionScope.id}" readonly>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Old Password</label> <input
														type="password" name="oldPassword" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">New Password</label> <input
														type="password" name="newPassword" class="form-control">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group label-floating">
													<label class="control-label">Confirm Password</label> <input
														type="password" name="confirmPassword"
														class="form-control">
												</div>
											</div>
										</div>
										<c:if test="${error != null}">
											<div class="row">
												<div class="col-md-12 text-danger">
													<span>${error}</span>
												</div>
											</div>
										</c:if>
										<button type="submit" class="btn btn-primary pull-right"
											data-background-color="green">Change</button>
										<div class="clearfix"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<%@ include file="footer.jsp"%>
		</div>
	</div>
</body>

<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/jquery-validate.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapValidator.min.js"></script>
<script type="text/javascript"
	src="resources/js/bootstrapFormValidate.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>

</html>
