<nav class="navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#pablo" class="dropdown-toggle"
					data-toggle="dropdown"> <i class="material-icons">person</i>
						<p class="hidden-lg hidden-md">Profile</p>
				</a>
					<ul class="dropdown-menu">
						<li><a href="logout">Logout</a></li>
						<li><a href="getUser?emailId=${sessionScope.id}">My
								profile</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
</nav>
