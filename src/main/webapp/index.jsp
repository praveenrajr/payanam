<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    response.setHeader("Cache-Control", "no-cache");
    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
%>
<c:if test="${sessionScope.id != null}">
	<c:redirect url="home" />
</c:if>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Payanam</title>

<meta
	content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
	name='viewport' />
<meta name="viewport" content="width=device-width" />

<!-- Bootstrap core CSS     -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet" />

<!--  Material Dashboard CSS    -->
<link href="resources/css/material-dashboard.css" rel="stylesheet" />

<!--     Fonts and icons     -->
<link href="resources/css/font.css" rel="stylesheet">
<link href="resources/css/materialFont.css" rel="stylesheet"
	type="text/css">
</head>

<body>
	<div class="wrapper">
		<header class="header">
			<div class="logo"></div>
		</header>
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="card col-md-4">
							<div class="card-content">
								<form method="post" action="login">
									<div class="row">
										<div class="col-md-12">
											<div class="form-group background-image="
												green" label-floating>
												<label class="control-label">E-Mail</label> <input
													type="email" name="emailId" class="form-control">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<div class="form-group label-floating">
												<label class="control-label">Password</label> <input
													type="password" name="password" class="form-control"
													required="required">
											</div>
										</div>
									</div>
									<c:if test="${param.error != null}">
										<div class="row">
											<div class="col-md-12 text-danger">
												<span>${param.error}</span>
											</div>
										</div>
									</c:if>
									<c:if test="${param.success != null}">
										<div class="row">
											<div class="col-md-12 text-success">
												<span>${param.success}</span>
											</div>
										</div>
									</c:if>
									<button type="submit" class="btn btn-primary pull-left"
										data-background-color="green">Login</button>
									<div class="clearfix"></div>
								</form>
								New user?<a href="registrationForm">Register here</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->
<script type="text/javascript" src="resources/js/jquery.min.js"></script>
<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
<script src="resources/js/material.min.js" type="text/javascript"></script>

<!--  Charts Plugin -->
<script src="resources/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="resources/js/bootstrap-notify.js"></script>

<!-- Material Dashboard javascript methods -->
<script src="resources/js/material-dashboard.js"></script>
</html>
