$(document)
		.ready(
				function() {
					var date = new Date();
					date.setDate(date.getDate());
					$('.datepicker').datepicker({
						startDate : date
					});
					$('#searchValidate')
							.bootstrapValidator(
									{
										feedbackIcons : {
											valid : 'glyphicon glyphicon-ok',
											invalid : 'glyphicon glyphicon-remove',
											validating : 'glyphicon glyphicon-refresh'
										},
										fields : {
											from : {
												validators : {
													notEmpty : {
														message : 'Please enter from location'
													}
												}
											},
											to : {
												validators : {
													different : {
														field : 'from',
														message : 'From and To can not be same'
													},
													notEmpty : {
														message : 'Please enter to location'
													}
												}
											},
										}
									}).on('success.form.bv', function(e) {

								// Get the form instance
								var $form = $(e.target);

								// Get the BootstrapValidator instance
								var bv = $form.data('bootstrapValidator');
							});
				});