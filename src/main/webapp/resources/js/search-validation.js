	$(document).ready(function(){
	  	   $('#searchForm').validate({
	  		   rules: {
	  			   from:{
	  				   required:true,
	  			   },
	  			   to:{
	  				   required:true,
	  			   },
	  			   tripDate: {
	  				   required:true,
	  				   date:true
	  			   }
	  		   },
	  		   messages: {
	  			  from:{
	  				  message:"Select start location"
	  			  },
	  			  to:{
	  				  message:"Select destination"
	  			  },
	  			  tripDate:{
	  				  message:"Enter travel date please"
	  			  }
	  		   }
	  	   });
	  	});