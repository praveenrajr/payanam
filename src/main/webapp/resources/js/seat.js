var price = 0;
var total = 0;
$(document).ready(function() {
	console.log("ready!");
	price = parseInt($('#price').val());
});

function limit() {
	var count = 0;
	var boxes = document.getElementsByClassName('boxes');
	for (var i = 0; i < boxes.length; i++) {
		if (boxes[i].checked && boxes[i].name == this.name) {
			count++;
		}
	}
	if (this.checked && count <= this.getAttribute("data-max")) {
		total += price;
		console.log("price", price);
		console.log("total", total);
		$("#total").html(total);
	} else if (this.checked && count >= this.getAttribute("data-max")) {
		this.checked = false;
		total -= price;
		alert("Maximum of " + this.getAttribute("data-max") + ".");
	} else if (!this.checked) {
		total -= price;
		console.log("price", price);
		console.log("total", total);
		$("#total").html(total);
	}

}
window.onload = function() {
	var boxes = document.getElementsByClassName('boxes');
	for (var i = 0; i < boxes.length; i++) {
		boxes[i].addEventListener('change', limit, false);
	}

}

function checkSelection() {
	if ($(".boxes:checked").length > 0) {
		return true;
	}
	else{
	    alert("Choose atleast on seat.");
	    return false;
	}
	}