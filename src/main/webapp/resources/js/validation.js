         	$(document).ready(function(){
         	   $('#registerForm').validate({
         		   rules: {
         			   firstname:{
         				   required:true,
         				   maxlength:30
         			   },
         			   lastName:{
         				   required:true,
         				   maxlength:30
         			   },
         			   emailId: {
         				   required:true,
         				   email:true
         			   },
         			   password:{
         				   required:true,
         				   pattern:/^((?=.*\d)(?=.*[a-z])(?=.*[@#$%]).{6,20})$/,
         				   minlength: 6,
         				   maxlength:20
         			   }
         		   },
         		   messages: {
         			  firstName:"Enter your name",
         			  lastName:"Enter your last name",
         			  emailId: {
         				  required:"You forgot to enter your email id",
         				  email:"Enter valid email Id"
         			  },
         		      password: {
         		    	  required:"You forgot to enter the password",
         		    	  pattern:"Password must have atleast a number and special character",
         		    	  minlength:"Password must have alteast 6 character"
         		      }
         		   }
         	   });
         	});