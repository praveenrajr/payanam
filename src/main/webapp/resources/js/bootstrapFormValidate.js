$(document).ready(function() {
	$('.datepicker').datepicker({});
	$('#formValidate').bootstrapValidator({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			firstName : {
				validators : {
					regexp : {
						regexp : /^[a-zA-Z ]*$/,
						message : 'Name should be in characters'
					},
					notEmpty : {
						message : 'Please enter your First Name'
					}
				}
			},
			lastName : {
				validators : {
					regexp : {
						regexp : /^[a-zA-Z ]*$/,
						message : 'Name should be in characters'
					},
					notEmpty : {
						message : 'Please enter your Last Name'
					}
				}
			},
			DOB : {
				validators : {
					notEmpty : {
						message : 'Please enter valid Date of birth'
					}
				}
			},
			password : {
				validators : {
					stringLength : {
						min : 8,
						max : 30
					},
					notEmpty : {
						message : 'Please enter your Password'
					}
				}
			},
			confirmPassword : {
				validators : {
					identical : {
						field : 'password',
						message : 'Password does not match'
					},
					notEmpty : {
						message : 'Please confirm your Password'
					}
				}
			},
			emailId : {
				validators : {
					notEmpty : {
						message : 'Please enter your Email Address'
					},
					emailAddress : {
						message : 'Please enter a valid Email Address'
					}
				}
			},
			phoneNo : {
				validators : {
					regexp : {
						regexp : /^[0-9]*$/,
						message : 'Please check your phone number'
					},
					notEmpty : {
						message : 'Please enter your Contact No.'
					}
				}
			},
			gender : {
				validators : {
					notEmpty : {
						message : 'Please select your Department/Office'
					}
				}
			},
		}
	}).on('success.form.bv', function(e) {

		// Get the form instance
		var $form = $(e.target);

		// Get the BootstrapValidator instance
		var bv = $form.data('bootstrapValidator');
	});
});