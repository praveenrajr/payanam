package com.ideas2it.payanam.model;

import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import com.ideas2it.payanam.model.Location;

/**
 * <p>
 * {@link Route} represents the route details in which the {@link Bus} operates.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017
 *
 */
@Entity
@Table(name = "route")
@FilterDef(name = "dateFilter", parameters = @ParamDef(name = "filterParam", type = "date"))
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @ManyToOne
    @JoinColumn(name = "from_id")
    private Location from;
    @ManyToOne
    @JoinColumn(name = "to_id")
    private Location to;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "route")
    @Filter(name = "dateFilter", condition = "trip_date = :filterParam")
    @javax.persistence.OrderBy(value = "arrivalTime")
    private Set<BusRoute> busRoutes;

    /**
     * <p>
     * Default Constructor;
     * </p>
     */
    public Route() {
    }

    /**
     * <p>
     * Parameterized constructor that used to set the values for the attributes
     * in the route.
     * </p>
     * 
     * @param from
     *            Set the from location.
     * @param to
     *            Set the to location.
     */
    public Route(Location from, Location to) {
        this.from = from;
        this.to = to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getFrom() {
        return from;
    }

    public void setFrom(Location from) {
        this.from = from;
    }

    public Location getTo() {
        return to;
    }

    public void setTo(Location to) {
        this.to = to;
    }

    public Set<BusRoute> getBusRoutes() {
        return busRoutes;
    }

    public void setBusRoutes(Set<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
    }

}
