package com.ideas2it.payanam.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>
 * {@link BusRoute} represent the trip details of {@link Bus}.
 * </p>
 * 
 * @author Sharvesh
 *
 */
@Entity
@Table(name = "bus_route")
public class BusRoute implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "arrival_time")
    private Time arrivalTime;
    @Column(name = "departure_time")
    private Time departureTime;
    @Column(name = "trip_date")
    private Date tripDate;
    @Column(name = "avaliable_seats")
    private int availableSeats;
    @Column(name = "price")
    private int price;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bus_id")
    private Bus bus;
    @ManyToOne
    private Route route;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "busRoute")
    private Set<Booking> bookings;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public BusRoute(Bus bus, Route route) {
        super();
        this.bus = bus;
        this.route = route;
    }

    public BusRoute(Time arrivalTime, Time departureTime, Date tripDate,
            int price, Bus bus, Route route, Status status) {
        super();
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.tripDate = tripDate;
        this.price = price;
        this.bus = bus;
        this.route = route;
        this.status = status;
    }

    public Bus getBus() {
        return bus;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public BusRoute(Time arrivalTime, Time departureTime, Date tripDate,
            int availableSeats) {
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.tripDate = tripDate;
        this.availableSeats = availableSeats;
    }

    public BusRoute() {
    }

    public BusRoute(Time arrivalTime, Time departureTime,
            java.sql.Date tripDate, Integer price, Bus bus, Route route) {
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.route = route;
        this.tripDate = tripDate;
        this.price = price;
        this.bus = bus;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Time getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Time departureTime) {
        this.departureTime = departureTime;
    }

    public Date getTripDate() {
        return tripDate;
    }

    public void setTripDate(Date tripDate) {
        this.tripDate = tripDate;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

}
