package com.ideas2it.payanam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * {@link BusType} represents the type details of the {@link Bus}.
 * </p>
 * 
 * @author Sharvesh created on 29-08-2017.
 *
 */
@Entity
@Table(name = "bus_type")
public class BusType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "bus_type")
    private String busType;

    /**
     * <p>
     * Default constructor
     * </p>
     */
    public BusType() {

    }

    /**
     * <p>
     * Parameterized constructor to set the values for the attributes.
     * </p>
     * 
     * @param id
     *            To set id for the bus type.
     * @param busType
     *            To set different type of bus types.
     */
    public BusType(int id, String busType) {
        super();
        this.id = id;
        this.busType = busType;
    }

    public BusType(int id) {
        this.id = id;
    }

    /**
     * <p>
     * Get the id of bus type.
     * </p>
     * 
     * @return Id that set to this bus type.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>
     * Sets the id for the bus type
     * </p>
     * 
     * @param id
     *            Id to set for bus type.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>
     * Get the type of bus types.
     * </p>
     * 
     * @return BusType that sets to the types.
     */
    public String getBusType() {
        return busType;
    }

    /**
     * <p>
     * Set the Bus type
     * </p>
     * 
     * @param busType
     *            Bus type that set for the types.
     */
    public void setBusType(String busType) {
        this.busType = busType;
    }
}
