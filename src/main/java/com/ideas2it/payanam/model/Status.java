package com.ideas2it.payanam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * {@link Status} represents the status information of the {@link Bus},
 * {@link User}, {@link Booking}, {@link BookingDetails}
 * </p>
 * 
 * @author Sharvesh created on 29-08-2017.
 *
 */
@Entity
@Table(name = "status")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "status")
    private String status;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public Status() {

    }

    public Status(String status) {
        this.status = status;
    }

    /**
     * <p>
     * Get the id of status.
     * </p>
     * 
     * @return Id that set for the status.
     */
    public int getId() {
        return id;
    }

    public Status(int id) {
        this.id = id;
    }

    /**
     * <p>
     * Parameterized constructor that set the value for the attributes
     * </p>
     * 
     * @param id
     *            Id to set for the status.
     * @param status
     *            Status that set for the different types of status.
     */
    public Status(int id, String status) {
        this.id = id;
        this.status = status;
    }

    /**
     * <p>
     * Sets the id
     * </p>
     * 
     * @param id
     *            The id to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>
     * Gets the status
     * </p>
     * 
     * @return Status that set.
     */
    public String getStatus() {
        return status;
    }

    /**
     * <p>
     * Sets the status
     * </p>
     * 
     * @param status
     *            Status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
