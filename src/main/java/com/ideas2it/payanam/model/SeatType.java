package com.ideas2it.payanam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * {@link SeatType} represents the type info of the seat.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017.
 *
 */
@Entity
@Table(name = "seat_type")
public class SeatType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "seat_type")
    private String seatType;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public SeatType() {
    }

    /**
     * <p>
     * Parameterized constructor to set the values for id and the seat types.
     * </p>
     * 
     * @param id
     *            The id to set the values.
     * @param seatType
     *            Seat type to set the values.
     */
    public SeatType(int id, String seatType) {
        this.id = id;
        this.seatType = seatType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

}
