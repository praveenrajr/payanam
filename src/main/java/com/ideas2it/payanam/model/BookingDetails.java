package com.ideas2it.payanam.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.Seat;

/**
 * <p>
 * {@link BookingDetails} represents the booking details of each passengers and
 * seat information in a {@link Booking}.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017.
 *
 */
@Entity
@Table(name = "booking_details")
public class BookingDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "passenger_name")
    private String passengerName;
    @Column(name = "passenger_age")
    private int age;
    @Column(name = "passenger_gender")
    private String gender;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "booking_id", nullable = false)

    private Booking booking;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "seat_id")
    private Seat seat;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public BookingDetails() {
    }

    public BookingDetails(String passengerName, int age, String gender,
            Booking booking, Seat seat) {
        super();
        this.passengerName = passengerName;
        this.age = age;
        this.gender = gender;
        this.booking = booking;
        this.seat = seat;
    }

    /**
     * <p>
     * Parameterized constructor to set the values for booking details.
     * </p>
     * 
     * @param passengerName
     *            name of passenger in the booking details.
     * @param age
     *            age of the passenger in the booking details.
     * @param gender
     *            gender of the passenger.
     * @param seat
     *            seat details of the booking.
     * @param status
     *            status of the bus route (trip).
     */
    public BookingDetails(String passengerName, String gender, int age,
            Seat seat, Status status) {
        super();
        this.passengerName = passengerName;
        this.age = age;
        this.gender = gender;
        this.status = status;
        this.seat = seat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
