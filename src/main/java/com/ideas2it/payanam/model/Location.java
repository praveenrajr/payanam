package com.ideas2it.payanam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * {@link Location} represents the geographical place details.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017.
 *
 */
@Entity
@Table(name = "location")
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "location")
    private String location;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public Location() {
    }

    /**
     * <p>
     * Parameterized constructor used to set the values for id and the
     * locations.
     * </p>
     * 
     * @param id
     *            Id to set the values.
     * @param location
     *            Set the values for the location.
     */
    public Location(int id, String location) {
        this.id = id;
        this.location = location;
    }

    public Location(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
