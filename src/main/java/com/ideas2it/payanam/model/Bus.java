package com.ideas2it.payanam.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Filter;
import org.omg.PortableInterceptor.TRANSPORT_RETRY;

import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Status;

/**
 * <p>
 * {@link Bus} represents the details of the bus which is a transport medium.
 * </p>
 * 
 * @author Sharvesh created on 29-08-2017.
 *
 */
@Entity
@Table(name = "bus")
public class Bus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private int id;
    @Column(name = "reg_no", unique = true, nullable = false)
    private String regNo;
    @Column(name = "name")
    private String name;
    @Column(name = "total_seats")
    private int totalSeats;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private BusType busType;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;
    @OneToMany(mappedBy = "bus", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<BusRoute> busRoutes;
    @OneToMany(mappedBy = "bus", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Seat> seats;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public Bus() {

    }

    /**
     * <p>
     * Parameterized constructor to set the values for the attributes.
     * </p>
     * 
     * @param regNo
     *            Registration number to set for bus to identity
     * @param name
     *            Name to set for the bus.
     * @param totalSeats
     *            Total seats to set for bus.
     * @param busType
     *            Bus type to set for bus to define which type.
     * @param status
     *            Status to set for bus to define.
     */
    public Bus(String regNo, String name, int totalSeats, BusType busType,
            Status status) {
        this.regNo = regNo;
        this.name = name;
        this.totalSeats = totalSeats;
        this.busType = busType;
        this.status = status;
    }

    public Bus(String name, int totalSeats, BusType busType, Status status) {
        this.name = name;
        this.totalSeats = totalSeats;
        this.busType = busType;
        this.status = status;
    }

    public Set<BusRoute> getBusRoutes() {
        return busRoutes;
    }

    public void setBusRoutes(Set<BusRoute> busRoutes) {
        this.busRoutes = busRoutes;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    /**
     * <p>
     * To get the id of bus.
     * </p>
     * 
     * @return Id that set to this bus.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>
     * Sets the id.
     * </p>
     * 
     * @param id
     *            The id to set.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>
     * To get the registration number of bus.
     * </p>
     * 
     * @return Registered number that set to this bus
     */
    public String getRegNo() {
        return regNo;
    }

    /**
     * <p>
     * Sets the registration number.
     * </p>
     * 
     * @param regNo
     *            The registration number to set.
     */
    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    /**
     * <p>
     * To get the name of bus.
     * </p>
     * 
     * @return Name that set to this bus
     */
    public String getName() {
        return name;
    }

    /**
     * <p>
     * Sets the name.
     * </p>
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>
     * To get the total seats of bus.
     * </p>
     * 
     * @return Total seats that set to this bus.
     */
    public int getTotalSeats() {
        return totalSeats;
    }

    /**
     * <p>
     * Sets the total seats.
     * </p>
     * 
     * @param totalSeats
     *            The total seats to set.
     */
    public void setTotalSeats(int totalSeats) {
        this.totalSeats = totalSeats;
    }

    /**
     * <p>
     * To get the bus type of bus.
     * </p>
     * 
     * @return Bus type that set to this bus.
     */
    public BusType getBusType() {
        return busType;
    }

    /**
     * <p>
     * Sets the bus type.
     * </p>
     * 
     * @param busType
     *            The bus type to set.
     */
    public void setBusType(BusType busType) {
        this.busType = busType;
    }

    /**
     * <p>
     * To get the status of bus.
     * </p>
     * 
     * @return Status that set to this bus.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * <p>
     * Sets the status.
     * </p>
     * 
     * @param status
     *            The status to set.
     */
    public void setStatus(Status status) {
        this.status = status;
    }
}
