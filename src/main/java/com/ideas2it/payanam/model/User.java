package com.ideas2it.payanam.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.payanam.model.Role;
import com.ideas2it.payanam.model.Status;

/**
 * <p>
 * {@link User} represents the details of the user of different {@link Role}.
 * </p>
 * 
 * @author Sachidhanandhan.S created on 29-08-2017
 *
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, updatable = false)
    private int id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email_id", unique = true, nullable = false, updatable = false)
    private String emailId;
    @Column(name = "phone_no", unique = true, nullable = false)
    private String phoneNo;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "age", nullable = false)
    private int age;
    @Column(name = "dob", nullable = false)
    private String DOB;
    @Column(name = "gender", nullable = false)
    private String gender;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private Role role;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public User() {
    }

    public User(String firstName, String lastName, String phoneNo, String DOB,
            String emailId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNo = phoneNo;
        this.DOB = DOB;
        this.emailId = emailId;
    }

    public User(int id, String firstName, String lastName, String emailId,
            String phoneNo, String password, int age, String DOB, String gender,
            Role role, Status status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.password = password;
        this.age = age;
        this.DOB = DOB;
        this.gender = gender;
        this.role = role;
        this.status = status;
    }

    public User(String firstName, String lastName, String emailId,
            String phoneNo, String password, int age, String DOB, String gender,
            Role role, Status status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.phoneNo = phoneNo;
        this.password = password;
        this.age = age;
        this.DOB = DOB;
        this.gender = gender;
        this.role = role;
        this.status = status;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Role getRole() {
        return role;
    }

    public Status getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
