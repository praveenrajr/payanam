package com.ideas2it.payanam.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.model.SeatType;

/**
 * <p>
 * {@link Seat} represents the seat details of the {@link Bus}.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017.
 *
 */
@Entity
@Table(name = "seat")
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "seat_no")
    private int seatNo;
    @ManyToOne
    @JoinColumn(name = "bus_id", nullable = false)
    private Bus bus;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private SeatType seatType;

    /**
     * <p>
     * Default constructor.
     * </p>
     */
    public Seat() {
    }

    /**
     * <p>
     * Parameterized constructor use to set the values for the seat attributes.
     * </p>
     * 
     * @param seatNo
     *            Seat number to set for the seat.
     * @param bus
     *            Seat that to be in bus.
     * @param status
     *            Set the status about the seat.
     * @param seatType
     *            Type of seat that set for the seat.
     */
    public Seat(int seatNo, Bus bus, Status status, SeatType seatType) {
        super();
        this.seatNo = seatNo;
        this.bus = bus;
        this.status = status;
        this.seatType = seatType;
    }

    public Seat(int seatNo) {
        this.seatNo = seatNo;
    }

    public Seat(int seatNo, Bus bus) {
        this.seatNo = seatNo;
        this.bus = bus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(int seatNo) {
        this.seatNo = seatNo;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }

}
