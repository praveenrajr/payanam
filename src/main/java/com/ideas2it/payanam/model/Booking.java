package com.ideas2it.payanam.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <p>
 * Getting and setting the booking details.
 * </p>
 * 
 * @author Sharvesh Created on 29-08-2017.
 *
 */
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private int id;
    @Column(name = "booking_date")
    private Date bookingDate;
    @Column(name = "ticket_id")
    private String ticketId;
    @ManyToOne
    @JoinColumn(name = "bus_route_id")
    private BusRoute busRoute;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;
    @OneToMany(mappedBy = "booking", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<BookingDetails> bookingDetails;

    /**
     * <p>
     * Default constructor
     * </p>
     */
    public Booking() {
    }

    /**
     * <p>
     * Parameterized constructor that used to set the values for the booking
     * attributes.
     * </p>
     * 
     * @param ticketId
     *            ticket for the booking which is unique and shared to both user
     *            and admin.
     * @param bookingDate
     *            date of the booking.
     * @param travelTime
     *            Set the time of travel.
     * @param route
     *            object of the persistent class {@link Route} which denotes the
     *            souurce and destination of the travel.
     * @param user
     *            object of the persistent class User which denotes a user
     *            details
     * @param status
     *            status of the booking details.
     */
    public Booking(String ticketId, Date bookingDate, BusRoute busRoute,
            User user, Status status) {
        this.bookingDate = bookingDate;
        this.busRoute = busRoute;
        this.ticketId = ticketId;
        this.user = user;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Set<BookingDetails> getBookingDetails() {
        return bookingDetails;
    }

    public void setBookingDetails(Set<BookingDetails> bookingDetails) {
        this.bookingDetails = bookingDetails;
    }

    public User getUser() {
        return user;
    }

    public BusRoute getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(BusRoute busRoute) {
        this.busRoute = busRoute;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

}
