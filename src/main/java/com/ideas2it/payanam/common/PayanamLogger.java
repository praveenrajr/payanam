package com.ideas2it.payanam.common;

import java.lang.Exception;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class PayanamLogger {
    private static Logger logger = LogManager.getLogger(Logger.class.getName());

    /**
     * <p>
     * Takes the throwable object and logs it to the log file in error level.
     * 
     * @param e
     *            The exception which is to logged in log file.
     */
    public static void log(Exception e) {
        logger.error(e.getMessage(), e);
    }
}
