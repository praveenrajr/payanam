package com.ideas2it.payanam.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ideas2it.payanam.exception.InvalidInputException;

/**
 * It validates name, phone number, DOB, email id and also finds age, current
 * date and checks whether string is empty or not.
 * 
 * @author Sachidhanandhan.S created on 30-08-2017
 *
 */
public class CommonUtil {

    /**
     * Check whether the name contains only alphabets.
     * 
     * @param name
     *            - name to be validated.
     * @return - true if it as only alphabets.
     */
    public static boolean isName(String name) {
        return Pattern.matches("[a-zA-Z]+", name);
    }

    /**
     * Checks whether the phone number is in correct format.
     * 
     * @param phoneNo
     *            - phone number to be validated.
     * @return - true if phone number matches correct format.
     */
    public static boolean isPhoneNo(String phoneNo) {
        return phoneNo.matches(("\\d{10}|\\d{11}|\\d{7}"));
    }

    /**
     * Checks whether the email id is in correct format.
     * 
     * @param emailId
     *            - email id to be validated.
     * @return - true if email id matches correct format.
     */
    public static boolean isEmailId(String emailId) {
        Pattern emailNamePattern = Pattern
                .compile("^[_A-Za-z0-9-]+(\\.[_A-Za" + "-z0-9-]+)*@[A-Za-z0-9"
                        + "]+(\\.[A-Za-z0-9]+)*(" + "\\.[A-Za-z]{2,})$");
        Matcher match = emailNamePattern.matcher(emailId);
        return match.matches();
    }

    /**
     * Checks whether the value contains only number.
     * 
     * @param value
     *            - value to be validated.
     * @return boolean - true if it as only number.
     */
    public static boolean isNumber(String value) {
        return value.matches("\\d+");
    }

    /**
     * Checks whether the DOB is in correct format and finds whether the given
     * DOB satisfies the condition.
     * 
     * @param date
     *            - date to be validated.
     * @return boolean - true if it matches correct DOB format and satisfies the
     *         condition, if not false.
     * @throws InvalidInputException
     *             if date is invalid
     */
    public static boolean isDate(String date) throws InvalidInputException {
        return (getPeriod(date) > 13) && (getPeriod(date) < 80);
    }

    /**
     * To find the current date.
     * 
     * @return date of the current day
     */
    public static String findDate() {
        Date date = new Date();
        return String.format("%tc", date);
    }

    /**
     * Calculates difference between the current date and the given date
     * 
     * @param value
     *            - date of birth of user
     * @return difference between given date and current date
     * @throws InvalidInputException
     *             If date is invalid
     */
    public static int getPeriod(String value) throws InvalidInputException {
        try {
            Date date = new SimpleDateFormat(Constants.DATE_FORMAT)
                    .parse(value);
            Instant instant = Instant.ofEpochMilli(date.getTime());
            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant,
                    ZoneId.systemDefault());
            LocalDate localDate = localDateTime.toLocalDate();
            LocalDate currentDate = LocalDate.now();
            return Period.between(localDate, currentDate).getYears();
        } catch (ParseException e) {
            throw new InvalidInputException(Constants.DATE_INVALID, e);
        }
    }

    /**
     * It returns current date and time.
     * 
     * @return date time format
     */
    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    /**
     * It returns difference between current date and the given date in days.
     * 
     * @param date
     *            input date to calculate difference in days
     * @return difference in days between two dates.
     */
    public static long daysBetween(Date date) {
        return Math.abs(date.getTime() - new Date().getTime());
    }
}
