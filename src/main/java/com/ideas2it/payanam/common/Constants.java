package com.ideas2it.payanam.common;

/**
 * Collected constants of general utility.
 *
 * @author Rajasekar created on 01-09-2017
 *
 */
public class Constants {
    public static final String TIME_FORMAT = "hh:mm";
    public static final String ARRIVAL_TIME = "arrivalTime";
    public static final String DEPARTURE_TIME = "departureTime";
    public static final String CANCELLED = "cancelled";
    public static final String GET_USER_AND_TRIP_DETAILS = "/getUserAndTrip";
    public static final String NAME = "name";
    public static final String NO_OF_SEATS = "noOfSeats";
    public static final String BUS_TYPE_ID = "busTypeId";
    public static final String BUS_TYPES = "busTypes";
    public static final String BUS = "bus";
    public static final String UPDATE_BUS = "/updateBus";
    public static final String DELETE_BUS = "/deleteBus";
    public static final String REG_NO = "regNo";
    public static final String BUS_ID = "busId";
    public static final String BUS_EXISTS = "Bus already exists for the given registration no";
    public static final String CONNECTION_FAILURE = "Something went wrong please try again later.!";
    public static final String BUS_ADD_SUCCESS = "Bus added successfully";
    public static final String ID = "id";
    public static final String DELETED = "deleted";
    public static final String SERIAL = "serial";
    public static final String MESSAGE = "message";
    public static final String SUCCESS = "success";
    public static final String STATUS = "status";
    public static final String ROLE = "role";
    public static final String ERROR = "error";
    public static final String DEPRECATION = "deprecation";
    public static final String DATE_FORMAT = "MM/dd/yyyy";
    public static final String DATE_INVALID = "Entered invalid date.";
    public static final String NO_BUSES_OPERATING = "No buses operating in this"
            + " for the given day.";
    public static final String NO_LOCATION_FOUND = "No location found";
    public static final String NO_BUS_TYPE = "No bus type found";
    public static final String BUS_FETCH_FAILED = "Sorry couldn't get buses.";
    public static final String EXCEPTION_ROUTE_READ = "Exception while trying "
            + "to retrieve route " + "detail with trip details";
    public static final String EXCEPTION_LOCATION_READ = "Exception while retrieving"
            + " all locations";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String BUSES = "Buses";
    public static final String SEARCH = "search";
    public static final String BUS_ROUTES = "BusRoutes";
    public static final String BUS_ROUTE_ID = "busRouteId";
    public static final String LOCATIONS = "locations";
    public static final String TRIP_DATE = "tripDate";
    public static final String BOOKED_SEATS = "bookedSeats";
    public static final String SEATS = "Seats";

    /**
     * Route constants
     */
    public static final String TICKET_CANCEL = "ticketCancel";
    public static final String ADD_USER = "/addUser";
    public static final String UPDATE_USER = "/updateUser";
    public static final String GET_USER = "/getUser";
    public static final String GET_ALL_USER = "/getAllUser";
    public static final String CHANGEPASSWORD = "/changePassword";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String REDIRECT = "redirect:/";
    public static final String REDIRECT_HOME = "redirect:/home";
    public static final String ROUTE_HOME = "/home";
    public static final String ROUTE_SEARCH = "/search";
    public static final String ROUTE_REGISTRATION_FORM = "/registrationForm";
    public static final String ROUTE_SEATS = "/seats";
    public static final String MY_TRIPS = "/myTrips";
    public static final String ROUTE_GET_BUS_REGNO = "getbusbyRegNo";
    public static final String ROUTE_ADD_BUS = "addBus";
    public static final String GET_USER_AND_TRIP = "getUserAndTrip";
    public static final String ADD_BUS_TRIP = "addTrip";

    /**
     * Bean constants
     */
    public static final String BEAN_LOCATION_DAO = "locationDAO";
    public static final String BEAN_ROUTE_DAO = "routeDAO";
    public static final String BEAN_ROUTE_SERVICE = "routeService";
    public static final String BEAN_LOCATION_SERVICE = "locationService";
    public static final String BEAN_BUS_SERVICE = "busService";
    public static final String USER_SERVICE = "userService";
    public static final String USER_DAO = "userDao";
    public static final String BEAN_BUS_ROUTE_DAO = "busRouteDAO";
    public static final String BEAN_BUS_DAO = "busDAO";
    public static final String BEAN_BUS_ROUTE_SERVICE = "busRouteService";
    public static final String BEAN_BUS_TYPE_DAO = "busTypeDao";
    public static final String BOOKING_DAO = "bookingDao";
    public static final String BOOKING_SERVICE = "bookingService";
    public static final String USER_ADMIN_VIEW = "user-admin-view";

    /**
     * User Constants
     */
    public static final String USER = "user";
    public static final String PHONENO = "phoneNo";
    public static final String DOB = "DOB";
    public static final String USERS = "users";
    public static final String FIRSTNAME = "firstName";
    public static final String SUCCESS_PASSWORD = "Password changed successfully, login to continue";
    public static final String LASTNAME = "lastName";
    public static final String EMAILID = "emailId";
    public static final String PASSWORD = "password";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String REGISTRATION = "registration";
    public static final String PHONE_EXIST = "phone number already taken";
    public static final String EMAIL_EXIST = "E-mail already taken";
    public static final String SUCCESS_UPDATE = "Successfully updated";
    public static final String PASSWORD_INCORRECT = "Entered incorrect password";
    public static final String INVALID_NAME = "Name should be in characters";
    public static final String INVALID_EMAIL_ID = "Invalid email id";
    public static final String INVALID_PHONE_NO = "Invalid phone number";
    public static final String BOOKED = "Booked";
    public static final String BUS_ROUTE = "busRoute";
    public static final String BUS_DASHBOARD = "bus-dashboard";
    public static final String USER_DASHBOARD = "user-dashboard";
    public static final String BOOKING = "booking";
    public static final String AGE = "age";
    public static final String GENDER = "gender";

    /**
     * Passengers
     */
    public static final String NO_OF_PASSENGERS = "noOfPassengers";
    public static final String PASSENGERS = "passengers";
    public static final String PASSENGERS_FORM = "passengers-form";
    public static final String SELECTED_SEATS = "SelectedSeats";
    public static final String NO_OF_PASSENGER = "noOfPassenger";
    public static final String SEAT = "seat";
    public static final String AVAILABLE = "Available";

    /**
     * Booking
     */
    public static final String CANCEL_SUCCESS = "Ticket canceled successfully";
    public static final String TICKET_SUCCESS = "Ticket booked successfully";
    public static final String TICKET_ID = "ticketId";
    public static final String PASSENGER_ID = "passengerId";
    public static final String UPCOMING_TRIPS = "upcomingTrips";
    public static final String PAST_TRIPS = "pastTrips";
    public static final String ALL_BOOKING_DETAILS = "allBookingDetails";
    public static final String ADD_BOOKING = "addBooking";
    public static final String GET_ALL_BOOKING_DETAILS = "getAllBookingDetails";
    public static final String BOOKING_DETAILS = "bookingDetails";
    public static final String GET_BOOKED_DETAIL = "getBookedDetail";

    /*
     * Login authenticate
     */
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String NO_CACHE_NO_STORE = "no-cache, no-store, must-revalidate";
    public static final String PRAGMA = "Pragma";
    public static final String NO_CACHE = "no-cache";
    public static final String EXPIRES = "Expires";
}
