package com.ideas2it.payanam.service;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Location;

/**
 * The LocationService interface exposes all the CRUD and business operation of
 * the location.
 * 
 * @author Rajasekar created on 30/08/2017
 */
public interface LocationService {

    /**
     * Gets all locations from the location record.
     * 
     * @return locations list of all locations available where buses operate.
     * @throws DbException
     *             if locations cannot be retrieved.
     * @throws PayanamException
     *             if the no location is available.
     */
    List<Location> getAllLocations() throws DbException, PayanamException;
}
