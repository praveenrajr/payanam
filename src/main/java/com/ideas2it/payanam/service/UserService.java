package com.ideas2it.payanam.service;

import java.util.List;
import java.util.Map;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.model.User;

/**
 * User service interface exposes create, update, display operations and
 * performs all business logic.
 * 
 * @author Sachidhananadhan.S created on 29-08-2017
 *
 */
public interface UserService {

    /**
     * It takes user details in user object to add in user record.
     * 
     * @param user
     *            - user details in object.
     * @return true if user added successfully in record.
     * @throws InvalidInputException
     *             - If user details are invalid.
     * @throws DbException
     *             - if user details can not be saved in record.
     */
    Map<String, Object> addUser(User user)
            throws InvalidInputException, DbException;

    /**
     * It takes user details to be updated in existing user details in record. *
     * 
     * @param phoneNo
     *            - unique phone number of the user to contact and access his
     *            account.
     * @param emailId
     *            - unique email id of the user to communicate and to access his
     *            account
     * @param DOB
     *            - date of birth of the user to find his age.
     * @param firstName
     *            - name of the user.
     * @param lastName
     *            - last name of the user
     * @return success message if updated else throws
     *         {@link InvalidInputException}
     * @throws InvalidInputException
     *             If user details are invalid and already existing in record.
     * @throws DbException
     *             If user details can not be updated in record.
     */
    String updateUser(String phoneNo, String emailId, String DOB,
            String firstName, String lastName)
            throws InvalidInputException, DbException;

    /**
     * It checks whether the value is available in record and it matches
     * password or not.
     * 
     * @param value
     *            - unique value, it can be phone number or emailId of the user.
     * @param password
     *            - password for the user to access his private account.
     * @return true if matches else false.
     * @throws DbException
     *             If value can not be fetched from record.
     */
    Map<String, Object> loginAuthenticate(String value, String password)
            throws DbException, InvalidInputException;

    /**
     * It checks whether old password matches the password in record, if matches
     * then updates the new password else throw {@link InvalidInputException}
     * 
     * @param emailId
     *            - unique email id of the user to access his account.
     * @param oldPassword
     *            - existing password in record used to login into account
     * @param newPassword
     *            - new password to be updated in record.
     * @return success message if password updated successfully.
     * @throws DbException
     *             if user can not update his password.
     * @throws InvalidInputException
     *             If user entered invalid password that does not matches
     *             existing password.
     */
    String changePassword(String emailId, String oldPassword,
            String newPassword) throws DbException, InvalidInputException;

    /**
     * It gets all users from record as list of user object.
     * 
     * @return list of user object.
     * @throws DbException
     *             If all users can not be fetched from record.
     */
    List<User> getAllUsers() throws DbException;

    /**
     * It takes email id to get user details.
     * 
     * @param emailId
     *            unique email id of the user to get his details.
     * @return User object contains user details.
     * @throws DbException
     *             If user details can not be fetched.
     */
    User getUser(String emailId) throws DbException;
}
