package com.ideas2it.payanam.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Bus;

/**
 * The BusService interface exposes all the CRUD and business operation of the
 * route.
 * 
 * @author Rajasekar created on 06/09/2017
 */
public interface BusService {

    /**
     * Gets all buses from the bus record which are currently active.
     * 
     * @return list of buses which are available in the bus record and currently
     *         active.
     * @throws DbException
     *             if buses cannot be retrieved.
     * @throws PayanamException
     *             if no active buses found in bus record.
     */
    List<Bus> getAllBuses() throws DbException, PayanamException;

    /**
     * Gets all buses from the bus record both active and inactive.
     * 
     * @return list of buses from the buses record.
     * @throws DbException
     *             if buses cannot be retrieved.
     * @throws PayanamException
     *             is no buses found in the bus record.
     */
    List<Bus> getAllAvailableBuses() throws DbException, PayanamException;

    /**
     * Adds a new bus to bus record after validating whether the bus is existing
     * by checking registration no.
     * 
     * @param bus
     *            object of the persistent class {@link Bus}
     * @return success or error message
     * @throws DbException
     *             if bus cannot be added to bus record.
     * @throws PayanamException
     *             if bus already exists in the bus record for the given
     *             registration number.
     */
    String addBus(Bus bus) throws DbException, PayanamException;

    /**
     * Gets bus from the bus record identified by the bus id which is unique for
     * every bus in the record.
     * 
     * @param id
     *            identifier of the bus in the record which is unique for every
     *            bus in the record.
     * @return {@link Bus} identified with the given bus id.
     * @throws PayanamException
     *             if no {@link Bus} is available for the given id.
     * @throws DbException
     *             if bus cannot be retrieved from the bus record
     */
    Bus getBusById(int id) throws DbException, PayanamException;

    /**
     * Takes the bus's Registration no to be deleted and deletes the {@link Bus}
     * from the {@link Bus} record after validating.
     * 
     * @param regNo
     *            Registration of the {@link Bus} which is unique for every
     *            {@link Bus} in the bus record.
     * @return return success message if bus and its routes is deleted
     * @throws PayanamException
     *             if no {@link Bus} is found for the given id.
     * @throws DbException
     *             if {@link Bus} for the given id does not exist.
     */
    String deleteBus(String regNo) throws DbException, PayanamException;

    /**
     * Gets bus from the bus record identified by the bus regNo which is unique
     * for every bus in the record.
     * 
     * @param regNo
     *            registration no of the bus which is unique for every bus in
     *            the record.
     * 
     * @return {@link Bus} identified with the given bus id.
     * @throws PayanamException
     *             if no {@link Bus} is available for the given id.
     * @throws DbException
     *             if bus cannot be retrieved from the bus record
     */
    Bus getBusByRegNo(String regNo) throws DbException, PayanamException;
}
