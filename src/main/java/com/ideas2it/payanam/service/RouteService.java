package com.ideas2it.payanam.service;

import java.util.Date;
import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;

/**
 * The RouteService interface exposes all the CRUD and business operation of the
 * route.
 * 
 * @author Rajasekar created on 30/08/2017
 */
public interface RouteService {

    /**
     * <p>
     * Searches for the trips available for the given route, retrieves the trip
     * and bus details as a list.
     * 
     * @param route
     *            Travel route of the bus which travels from source location to
     *            destination.
     * @param tripDate
     *            Date of the travel.
     * @return busRoutes list of trips which are operated in the particular
     *         route for the given date and constraints
     * @throws PayanamException
     *             if no route found for the given constraints.
     * @throws DbException
     *             if route cannot be retrieved.
     */
    List<BusRoute> search(Route route, Date tripDate)
            throws PayanamException, DbException;

    /**
     * <p>
     * Gets route from the route record matching the route from location and to
     * location constraints passed.
     * 
     * @param from
     *            {@link Location} start point of the route.
     * @param to
     *            {@link Location} destination of the route.
     * @return {@link Route} object of the persistent class Route
     * @throws PayanamException
     *             if no route found for the given constraints.
     * @throws DbException
     *             if {@link Route} cannot be retrieved
     */
    Route getRouteByFromTo(Location from, Location to)
            throws PayanamException, DbException;
}
