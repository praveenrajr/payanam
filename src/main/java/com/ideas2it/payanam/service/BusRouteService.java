package com.ideas2it.payanam.service;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;

/**
 * The RouteService interface exposes all the READ operation and business
 * operation of the route.
 * 
 * @author Rajasekar created on 30/08/2017
 */
public interface BusRouteService {

    /**
     * <p>
     * Gets bus trip from the bus route record for the given id which is unique
     * for every record.
     * 
     * @param id
     *            identifier of the BusRoute which is unique for every bus route
     *            (trip).
     * @return {@link BusRoute} found for the given id.
     * @throws DbException
     *             if busroute cannot be retrieved the record.
     * @throws PayanamException
     *             if no busroute is avaiable for the given id.
     */
    BusRoute getBusRouteById(int id) throws DbException, PayanamException;

    /**
     * <p>
     * Adds new trip to the bus route record for the given bus with bus
     * registration number and trip details.
     * 
     * @param busRoute
     *            object of the persistant class {@link BusRoute} to be added to
     *            the record
     * @throws DbException
     *             if bus route (trip) cannot be inserted.
     * @throws PayanamException
     *             if bus route already exists in the record.
     * @return success message when bus route (trip) is not inserted
     *         successfully.
     */
    String addBusRoute(BusRoute busRoute) throws DbException, PayanamException;
}
