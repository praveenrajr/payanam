package com.ideas2it.payanam.service;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Location;

/**
 * The BusTypeService interface exposes the Read the BusType.
 * 
 * @author Rajasekar created on 06/09/2017
 */
public interface BusTypeService {

    /**
     * Gets all bus types from the bus type record.
     * 
     * @return locations list of all locations available where buses operate.
     * @throws DbException
     *             if locations cannot be retrieved.
     * @throws PayanamException
     *             if the no location is available.
     */
    List<BusType> getAllBusTypes() throws DbException, PayanamException;
}
