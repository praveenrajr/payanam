package com.ideas2it.payanam.service;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Seat;

/**
 * Seat service interface exposes fetch operation from the seat record.
 * 
 * @author Sachidhananadhan.S created on 06-09-2017
 */
public interface SeatService {

    /**
     * Retrieves seat for the seat record for the given seat id.
     * 
     * @param id
     *            identifier of the particular seat in the seat record which is
     *            unique for every seat in the record.
     * @return seat seat found for the given seat id.
     * @throws DbException
     *             if seat cannot be retrieved.
     */
    Seat getSeatById(int id) throws DbException;
}
