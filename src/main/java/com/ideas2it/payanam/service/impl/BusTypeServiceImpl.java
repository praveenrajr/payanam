package com.ideas2it.payanam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusTypeDAO;
import com.ideas2it.payanam.dao.LocationDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.service.BusTypeService;

@Service("busTypeService")
/*
 * (non-Javadoc)
 * 
 * @see com.ideas2it.payanam.service.BusTypeService
 */
public class BusTypeServiceImpl implements BusTypeService {
    @Autowired
    BusTypeDAO busTypeDAO;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusTypeService#getAllBusTypes()
     */
    @Override
    public List<BusType> getAllBusTypes() throws DbException, PayanamException {
        List<BusType> busTypes = busTypeDAO.getAllBusTypes();
        if (null == busTypes) {
            throw new PayanamException(Constants.NO_LOCATION_FOUND);
        }
        return busTypes;
    }
}
