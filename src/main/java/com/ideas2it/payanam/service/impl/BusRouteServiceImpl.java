package com.ideas2it.payanam.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusRouteDAO;
import com.ideas2it.payanam.dao.RouteDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Route;
import com.ideas2it.payanam.service.BusRouteService;
import com.ideas2it.payanam.service.RouteService;

@Service(Constants.BEAN_BUS_ROUTE_SERVICE)
/*
 * (non-Javadoc)
 * 
 * @see com.ideas2it.payanam.service.BusRouteService
 */
public class BusRouteServiceImpl implements BusRouteService {
    @Autowired
    private BusRouteDAO busRouteDAO;
    @Autowired
    private RouteService routeService;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusRouteService#getBusRouteById(int)
     */
    @Override
    public BusRoute getBusRouteById(int id)
            throws DbException, PayanamException {
        BusRoute busRoute = busRouteDAO.getBusRouteById(id);
        if (null == busRoute) {
            throw new PayanamException("No trip found for the given id");
        }
        return busRoute;
    }

    public String addBusRoute(BusRoute busRoute)
            throws PayanamException, DbException {
        for (BusRoute existingBusRoute : routeService
                .search(busRoute.getRoute(), busRoute.getTripDate())) {
            if (existingBusRoute.getBus().getRegNo()
                    .equals(busRoute.getBus().getRegNo())) {
                throw new PayanamException(
                        "Bus route already exist for the bus, please enter a new route");
            }
        }
        busRoute.setAvailableSeats(busRoute.getBus().getTotalSeats());
        busRouteDAO.insertTrip(busRoute);
        return "success";
    }
}
