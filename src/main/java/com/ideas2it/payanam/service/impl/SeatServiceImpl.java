package com.ideas2it.payanam.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Seat;
import com.ideas2it.payanam.service.SeatService;
import com.ideas2it.payanam.dao.SeatDAO;

/**
 * It performs search operations on the seat details.
 * 
 * @author Sachidhanandhan.S created on 06-09-2017
 */
@Service("seatService")
public class SeatServiceImpl implements SeatService {
    @Autowired
    private SeatDAO seatDao;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.SeatService#getSeatById(int)
     */
    public Seat getSeatById(int id) throws DbException {
        return seatDao.getSeatById(id);
    }
}
