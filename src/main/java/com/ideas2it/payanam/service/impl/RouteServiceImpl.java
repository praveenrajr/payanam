package com.ideas2it.payanam.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.RouteDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;
import com.ideas2it.payanam.service.RouteService;

@Service(Constants.BEAN_ROUTE_SERVICE)
/*
 * (non-Javadoc)
 * 
 * @see com.ideas2it.payanam.service.RouteService
 */
public class RouteServiceImpl implements RouteService {
    @Autowired
    private RouteDAO routeDAO;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.RouteService#search(com.ideas2it.payanam.
     * model.Route, java.util.Date)
     */
    @Override
    public List<BusRoute> search(Route route, Date tripDate)
            throws DbException, PayanamException {
        List<BusRoute> availableTrips = new ArrayList<>();
        List<BusRoute> busRoutes = routeDAO.search(route, tripDate);
        if (null == busRoutes || busRoutes.isEmpty()) {
            throw new PayanamException(
                    "No buses available for the given date.!");
        } else {
            for (BusRoute busRoute : busRoutes) {
                if (!Constants.DELETED
                        .equals(busRoute.getStatus().getStatus())) {
                    availableTrips.add(busRoute);
                }
            }
        }
        return availableTrips;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.RouteService#getRouteByFromTo(com.ideas2it.
     * payanam.model.Location, com.ideas2it.payanam.model.Location)
     */
    @Override
    public Route getRouteByFromTo(Location from, Location to)
            throws PayanamException, DbException {
        Route route = routeDAO.getRouteByFromTo(from, to);
        if (null == route) {
            throw new PayanamException(
                    "No route found for the given source and destination.");
        }
        return route;
    }

}
