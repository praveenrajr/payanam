package com.ideas2it.payanam.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.CommonUtil;
import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BookingDao;
import com.ideas2it.payanam.dao.BusRouteDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.BookingDetails;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.model.User;
import com.ideas2it.payanam.service.BookingService;
import com.ideas2it.payanam.service.BusRouteService;
import com.ideas2it.payanam.service.UserService;

/**
 * It performs booking, canceling the booked tickets and gets upcoming and past
 * trips available in the record for the user.It also gets all the available
 * booking details available in the record.
 * 
 * @author Sachidhanandhan.S created on 04-09-2017
 */
@Service("bookingService")
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingDao bookingDao;
    @Autowired
    private UserService userService;
    @Autowired
    private BusRouteService busRouteService;
    @Autowired
    private BusRouteDAO busRouteDao;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BookingService#addBookingDetails(int,
     * java.lang.String)
     */
    @Override
    public Map<String, String> addBookingDetails(int busRouteId, String emailId,
            Set<BookingDetails> bookingDetails, int noOfPassenger)
            throws DbException, PayanamException {
        Map<String, String> tripMessage = new HashMap<>();
        String ticketId = UUID.randomUUID().toString();
        User user = userService.getUser(emailId);
        BusRoute busRoute = busRouteService.getBusRouteById(busRouteId);
        Booking booking = new Booking(ticketId, CommonUtil.getCurrentDate(),
                busRoute, user, new Status(3));
        for (BookingDetails bookingDetail : bookingDetails) {
            bookingDetail.setBooking(booking);
            bookingDetail.setStatus(new Status(6, Constants.BOOKED));
        }
        busRoute.setAvailableSeats(
                busRoute.getAvailableSeats() - noOfPassenger + 1);
        booking.setBookingDetails(bookingDetails);
        bookingDao.addBookingDetails(booking);
        busRouteDao.updateBusRoute(busRoute);
        tripMessage.put(Constants.STATUS, Constants.TICKET_SUCCESS);
        tripMessage.put(Constants.TICKET_ID, ticketId);
        return tripMessage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.BookingService#getBookingDetails(java.lang.
     * String)
     */
    @Override
    public Map<String, Set<Booking>> getUserTrips(String emailId)
            throws DbException {
        Map<String, Set<Booking>> userTrips = new HashMap<>();
        User user = userService.getUser(emailId);
        Set<Booking> bookings = new HashSet<>(
                bookingDao.retrieveUserTrips(user));
        Set<Booking> newBooking = new HashSet<>();
        Set<Booking> oldBooking = new HashSet<>();
        for (Booking booking : bookings) {
            if (booking.getStatus().getStatus().equals(Constants.AVAILABLE)
                    && 0 < CommonUtil
                            .daysBetween(booking.getBusRoute().getTripDate())) {
                newBooking.add(booking);
            }
            if (booking.getStatus().getStatus().equals(Constants.AVAILABLE)
                    && 0 > CommonUtil
                            .daysBetween(booking.getBusRoute().getTripDate())) {
                oldBooking.add(booking);
            }
        }
        userTrips.put(Constants.UPCOMING_TRIPS, newBooking);
        userTrips.put(Constants.PAST_TRIPS, oldBooking);
        return userTrips;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.BookingService#getBookedTicket(java.lang.
     * String, java.lang.String)
     */
    @Override
    public Booking getBookedTicket(String ticketId, String emailId)
            throws DbException, InvalidInputException {
        Booking booking = bookingDao.retrieveBooking(ticketId);
        if (null != booking
                && booking.getStatus().getStatus().equals(Constants.AVAILABLE)
                && booking.getUser().getEmailId().equals(emailId)) {
            if (CommonUtil
                    .daysBetween(booking.getBusRoute().getTripDate()) > 0) {
                return booking;
            }
            throw new InvalidInputException("Ticket id has been expired");
        }
        throw new InvalidInputException("please check your ticket id");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BookingService#cancelTicket(java.lang.
     * String, java.lang.String, int)
     */
    @Override
    public String cancelTicket(String ticketId, String emailId,
            String passengerId) throws DbException, InvalidInputException {
        Booking booking = getBookedTicket(ticketId, emailId);
        for (BookingDetails bookingDetail : booking.getBookingDetails()) {
            cancelPassengerTicket(booking, bookingDetail,
                    Integer.parseInt(passengerId));
        }
        BusRoute busRoute = busRouteDao
                .getBusRouteById(booking.getBusRoute().getId());
        busRoute.setAvailableSeats(booking.getBusRoute().getAvailableSeats());
        expireTicketId(booking);
        bookingDao.updateBooking(booking);
        busRouteDao.updateBusRoute(busRoute);
        return Constants.CANCEL_SUCCESS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BookingService#getAllBookingDetails()
     */
    @Override
    public List<Booking> getAllBookingDetails() throws DbException {
        return bookingDao.retrieveAllBookingDetails();
    }

    /**
     * It cancel passenger ticket using passenger id
     * 
     * @param booking
     *            booking details of the user.
     * @param bookingDetail
     *            booking details of the passenger
     * @param passengerId
     *            passenger id to cancel the ticket.
     */
    private void cancelPassengerTicket(Booking booking,
            BookingDetails bookingDetail, int passengerId) {
        if (passengerId == (bookingDetail.getSeat().getSeatNo())) {
            bookingDetail.setStatus(new Status(5, Constants.CANCELLED));
            booking.getBusRoute().setAvailableSeats(
                    booking.getBusRoute().getAvailableSeats() + 1);
            if (1 == booking.getBookingDetails().size()) {
                booking.setStatus(new Status(5, Constants.CANCELLED));
            }
        }
    }

    /**
     * It checks the status of all the available details in the booking details
     * are booked. If all the booking details are cancelled then it cancels the
     * user booking by setting the booking status to cancelled.
     * 
     * @param bookingn
     *            user booking details.
     */
    private void expireTicketId(Booking booking) {
        int noOfBooking = booking.getBookingDetails().size();
        for (BookingDetails bookingDetail : booking.getBookingDetails()) {
            if (bookingDetail.getStatus().getStatus()
                    .equals(Constants.CANCELLED)) {
                --noOfBooking;
            }
        }
        if (0 == noOfBooking) {
            booking.setStatus(new Status(5, Constants.CANCELLED));
        }
    }
}
