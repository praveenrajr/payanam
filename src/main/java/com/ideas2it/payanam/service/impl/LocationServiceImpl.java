package com.ideas2it.payanam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.LocationDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.service.LocationService;

@Service(Constants.BEAN_LOCATION_SERVICE)
/*
 * (non-Javadoc)
 * 
 * @see com.ideas2it.payanam.service.LocationService
 */
public class LocationServiceImpl implements LocationService {

    @Autowired
    LocationDAO locationDAO;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.LocationService#getAllLocations()
     */
    @Override
    public List<Location> getAllLocations()
            throws DbException, PayanamException {
        List<Location> locations = locationDAO.getAllLocations();
        if (null == locations) {
            throw new PayanamException(Constants.NO_LOCATION_FOUND);
        }
        return locations;
    }
}
