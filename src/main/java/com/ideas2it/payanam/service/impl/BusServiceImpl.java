package com.ideas2it.payanam.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Seat;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.service.BusService;

@Service(Constants.BEAN_BUS_SERVICE)
/*
 * (non-Javadoc)
 * 
 * @see com.ideas2it.payanam.service.BusService
 */
public class BusServiceImpl implements BusService {

    @Autowired
    BusDAO busDAO;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusService#getAllBuses()
     */
    @Override
    public List<Bus> getAllBuses() throws DbException, PayanamException {
        List<Bus> buses = busDAO.retrieveAllBuses();
        if (buses.isEmpty()) {
            throw new PayanamException("No buses found");
        }
        return buses;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusService#getAllAvailableBuses()
     */
    @Override
    public List<Bus> getAllAvailableBuses()
            throws DbException, PayanamException {
        Status status = new Status(1);
        List<Bus> buses = busDAO.retrieveAllAvailableBuses(status);
        if (buses.isEmpty()) {
            throw new PayanamException("No buses found");
        }
        return buses;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.BusService#addBus(com.ideas2it.payanam.model
     * .Bus)
     */
    @Override
    public String addBus(Bus bus) throws DbException, PayanamException {
        validator(bus);
        Set<Seat> seats = new HashSet<>();
        for (int index = 0; index < bus.getTotalSeats(); index++) {
            seats.add(new Seat(index, bus));
        }
        bus.setSeats(seats);
        busDAO.addBus(bus);
        return Constants.BUS_ADD_SUCCESS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusService#getBusById()
     */
    @Override
    public Bus getBusById(int id) throws DbException, PayanamException {
        Bus bus = busDAO.getBusById(id);
        if (null == bus) {
            throw new PayanamException("No bus found for the given bus id");
        }
        return bus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.BusService#deleteBus(int)
     */
    @Override
    public String deleteBus(String regNo) throws DbException, PayanamException {
        Bus bus = busDAO.getBusByRegNo(regNo);
        for (BusRoute busRoute : bus.getBusRoutes()) {
            if (busRoute.getBookings().isEmpty()) {
                bus.setStatus(new Status(4));
                busDAO.updateBus(bus);
            } else {
                busRoute.setStatus(new Status(4, Constants.DELETED));
            }
        }
        throw new PayanamException(
                "User has booked seats, hence the bus can not be deleted");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.BusService#getBusByRegNo(java.lang.String)
     */
    @Override
    public Bus getBusByRegNo(String regNo)
            throws DbException, PayanamException {
        Bus bus = busDAO.getBusByRegNo(regNo);
        if (null == bus) {
            throw new PayanamException("No bus found for the given bus reg no");
        }
        return bus;
    }

    /**
     * Takes bus object to checks if the reg number exist in record. If exist it
     * throws {@link InvalidInputException}
     * 
     * @param bus
     *            bus details as object to be validated.
     * @return
     * @throws PayanamException
     *             If bus details already exist in record.
     * @throws DbException
     *             If user details can not be fetched using emailId or phone
     *             number.
     */
    private Boolean validator(Bus bus) throws PayanamException, DbException {
        if (null != busDAO.getBusByRegNo(bus.getRegNo())) {
            throw new PayanamException(Constants.BUS_EXISTS);
        }
        return Boolean.TRUE;
    }

}
