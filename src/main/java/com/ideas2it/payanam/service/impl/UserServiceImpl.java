package com.ideas2it.payanam.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.payanam.common.CommonUtil;
import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.UserDao;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.model.User;
import com.ideas2it.payanam.service.UserService;

@Service(Constants.USER_SERVICE)
/**
 * It performs addition, update, validation, change password and checks existing
 * operations on the user details.
 * 
 * @author Sachidhanandhan.S created on 29-08-2017
 *
 */
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.UserService#addUser(com.ideas2it.payanam.
     * model.User)
     */
    @Override
    public Map<String, Object> addUser(User user)
            throws InvalidInputException, DbException {
        Map<String, Object> status = new HashMap<>();
        validateUserDetails(user.getPhoneNo(), user.getEmailId(), user.getDOB(),
                user.getFirstName(), user.getLastName());
        validator(user);
        user.setAge(CommonUtil.getPeriod(user.getDOB()));
        user.setPassword(passwordEncrypt(user.getPassword()));
        user.setStatus(new Status(1));
        status.put(Constants.STATUS, userDao.saveUser(user));
        status.put(Constants.ROLE, user.getRole().getRoleType());
        return status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.UserService#updateUser(com.ideas2it.payanam.
     * model.User)
     */
    @Override
    public String updateUser(String phoneNo, String emailId, String DOB,
            String firstName, String lastName)
            throws InvalidInputException, DbException {
        validateUserDetails(phoneNo, emailId, DOB, firstName, lastName);
        User oldUser = userDao.getUserByEmailId(emailId);
        if (!oldUser.getPhoneNo().equals(phoneNo)
                && null != userDao.getUserByPhoneNo(phoneNo)) {
            throw new InvalidInputException(Constants.PHONE_EXIST);
        }
        oldUser.setFirstName(firstName);
        oldUser.setLastName(lastName);
        oldUser.setAge(CommonUtil.getPeriod(DOB));
        oldUser.setDOB(DOB);
        oldUser.setPhoneNo(phoneNo);
        userDao.updateUser(oldUser);
        return Constants.SUCCESS_UPDATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.UserService#loginAuthenticate(java.lang.
     * String, java.lang.String)
     */
    @Override
    public Map<String, Object> loginAuthenticate(String value, String password)
            throws DbException, InvalidInputException {
        Map<String, Object> status = new HashMap<>();
        User user = userDao.getUserByValue(value);
        if (null != user
                && passwordEncrypt(password).equals(user.getPassword())) {
            status.put(Constants.STATUS, true);
            status.put(Constants.ROLE, user.getRole().getRoleType());
            return status;
        }
        status.put(Constants.STATUS, false);
        return status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.service.UserService#changePassword(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public String changePassword(String value, String oldPassword,
            String newPassword) throws InvalidInputException, DbException {
        User user = userDao.getUserByValue(value);
        if (passwordEncrypt(oldPassword).equals(user.getPassword())) {
            user.setPassword(passwordEncrypt(newPassword));
            userDao.updateUser(user);
            return Constants.SUCCESS_PASSWORD;
        }
        throw new InvalidInputException(Constants.PASSWORD_INCORRECT);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.UserService#getAllUsers()
     */
    @Override
    public List<User> getAllUsers() throws DbException {
        return userDao.retrieveAllUsers();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.service.UserService#getUser(java.lang.String)
     */
    @Override
    public User getUser(String emailId) throws DbException {
        return userDao.getUserByEmailId(emailId);
    }

    /**
     * It takes user object to checks if phone number and email Id exist in
     * record. If exist it throws {@link InvalidInputException}
     * 
     * @param user
     *            user details as object to be validated.
     * @throws InvalidInputException
     *             If user details already exist in record.
     * @throws DbException
     *             If user details can not be fetched using emailId or phone
     *             number.
     */
    private void validator(User user)
            throws InvalidInputException, DbException {
        boolean isExist = false;
        StringBuilder message = new StringBuilder();
        if (null != userDao.getUserByEmailId(user.getEmailId())) {
            isExist = true;
            message.append(Constants.EMAIL_EXIST);
        }
        if (null != userDao.getUserByPhoneNo(user.getPhoneNo())) {
            isExist = true;
            message.append(Constants.PHONE_EXIST);
        }
        if (isExist) {
            throw new InvalidInputException(message.toString());
        }
    }

    /**
     * It validates user details.If user details are incorrect it throws
     * {@link InvalidInputException}
     * 
     * @param phoneNo
     *            - unique phone number of the user to contact.
     * @param emailId
     *            - unique email id of the user for communication
     * @param DOB
     *            - date of birth of the user.
     * @param first_name
     *            - name of the user.
     * @param last_name
     *            - last name of the user.
     * @throws InvalidInputException
     *             If user details are invalid.
     */
    private void validateUserDetails(String phoneNo, String emailId, String DOB,
            String first_name, String last_name) throws InvalidInputException {
        boolean status = true;
        StringBuilder message = new StringBuilder();
        if (!CommonUtil.isName(first_name)) {
            status = false;
            message.append(Constants.INVALID_NAME);
        }
        if (!CommonUtil.isName(last_name)) {
            status = false;
            message.append(Constants.INVALID_NAME);
        }
        if (!CommonUtil.isEmailId(emailId)) {
            status = false;
            message.append(Constants.INVALID_EMAIL_ID);
        }
        if (!CommonUtil.isDate(DOB)) {
            status = false;
            message.append(Constants.DATE_INVALID);
        }
        if (!CommonUtil.isPhoneNo(phoneNo)) {
            status = false;
            message.append(Constants.INVALID_PHONE_NO);
        }
        if (!status) {
            throw new InvalidInputException(message.toString());
        }
    }

    /**
     * It encrypts the user password for security purpose.
     * 
     * @param password
     *            user password to login into his account.
     * @return encrypted password as string
     * @throws InvalidInputException
     *             if any exception occurred while encrypting the user password
     */
    private String passwordEncrypt(String password)
            throws InvalidInputException {

        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            byte[] passBytes = password.getBytes();
            messageDigest.reset();
            byte[] digested = messageDigest.digest(passBytes);
            StringBuffer encryptedPassword = new StringBuffer();
            for (int minLength = 0; minLength < digested.length; minLength++) {
                encryptedPassword.append(
                        Integer.toHexString(0xff & digested[minLength]));
            }
            return encryptedPassword.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new InvalidInputException(
                    "Exception occured while encrypting user password", e);
        }
    }
}
