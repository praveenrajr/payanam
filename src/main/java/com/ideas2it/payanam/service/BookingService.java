package com.ideas2it.payanam.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.BookingDetails;

/**
 * Booking service interface exposes add, update, display operations on booking
 * details
 * 
 * @author Sachidhanandhan.S created on 04-09-2017
 */
public interface BookingService {

    Map<String, String> addBookingDetails(int busRouteId, String emailId,
            Set<BookingDetails> bookingDetails, int noOfPassenger)
            throws PayanamException, DbException;

    /**
     * It takes user email Id and takes corresponding list of trips for the
     * user.
     * 
     * @param emailId
     *            unique email id of the user to get list of trips
     * @return list of user trips.
     * @throws DbException
     *             If list of trips can not be fetched from the record.
     */
    Map<String, Set<Booking>> getUserTrips(String emailId) throws DbException;

    /**
     * It gets a booked ticket using ticket id for the user from the record.
     * 
     * @param ticketId
     *            - unique ticket id of the trip
     * @param emailId
     *            - unique email id of the user.
     * @return booked ticket if available in the record if not returns null.
     * @throws InvalidInputException
     *             if email id is invalid and ticked id is expired.
     * @throws DbException
     *             if booked ticket can not be fetched from the record.
     */
    Booking getBookedTicket(String ticketId, String emailId)
            throws InvalidInputException, DbException;

    /**
     * It gets all the booking details of all the users from the record
     * 
     * @return list of booking details
     * @throws DbException
     *             if list of booking details can not fetched from the record.
     */
    List<Booking> getAllBookingDetails() throws DbException;

    /**
     * It cancels the booked ticket available in the record.
     * 
     * @param ticketId
     *            - unique ticket Id of the user trip to cancel
     * @param emailId
     *            - unique email id of the user to get booking detail
     * @param passengerId
     *            - to cancel the seat for the passenger.
     * @return success message if ticket as canceled successfully
     * @throws DbException
     *             If booking details of the user can not be fetched or ticke
     *             for the user can not be canceled
     * @throws InvalidInputException
     *             If email id or the ticket id is invalid.
     */
    String cancelTicket(String ticketId, String emailId, String passengerId)
            throws DbException, InvalidInputException;
}
