package com.ideas2it.payanam.exception;

import java.lang.Exception;

import com.ideas2it.payanam.common.Constants;

/**
 * User defined exception to handle all the exception that occurs due invalid
 * input at the runtime.
 * 
 * @author Sachidhanandhan.S created on 30-08-2017
 *
 */
@SuppressWarnings(Constants.SERIAL)
public class InvalidInputException extends Exception {
    public InvalidInputException() {
    }

    public InvalidInputException(Throwable cause) {
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
