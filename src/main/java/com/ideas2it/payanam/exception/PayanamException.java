package com.ideas2it.payanam.exception;

import com.ideas2it.payanam.common.Constants;

/**
 * Handles all the exceptions that could occur in the application.
 * 
 * @author Rajasekar created on 30/08/2017
 */
@SuppressWarnings(Constants.SERIAL)
public class PayanamException extends Exception {

    public PayanamException() {
        super();
    }

    public PayanamException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PayanamException(String message, Throwable cause) {
        super(message, cause);
    }

    public PayanamException(String message) {
        super(message);
    }

    public PayanamException(Throwable cause) {
        super(cause);
    }

}
