/**
 * 
 */
package com.ideas2it.payanam.exception;

import java.lang.Exception;

import com.ideas2it.payanam.common.Constants;

/**
 * user defined exception to handle all type of exception occurred at database.
 * 
 * @author Sachidhanandhan.S created on 30-08-2017
 *
 */
@SuppressWarnings(Constants.SERIAL)
public class DbException extends Exception {
    public DbException() {
    }

    public DbException(Throwable cause) {
    }

    public DbException(String message) {
        super(message);
    }

    public DbException(String message, Throwable cause) {
        super(message, cause);
    }
}
