package com.ideas2it.payanam.dao;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.BusRoute;

/**
 * The BusRouteDAO exposes all the CRUD operations related to the BusRoute.
 * 
 * @author Rajasekar created on 04/09/2017
 */
public interface BusRouteDAO {

    /**
     * <p>
     * Gets bus trip from the bus route record for the given id which is unique
     * for every record.
     * 
     * @param id
     *            id of the BusRoute which is unique for every bus route.
     * @return {@link BusRoute} found for the given id.
     * @throws DbException
     *             if bus route cannot be retrieved the record.
     */
    BusRoute getBusRouteById(int id) throws DbException;

    /**
     * 
     * It updates bus route details with the existing details in the record.
     * 
     * @param busRoute
     *            updated bus route details
     * @throws DbException
     *             If bus route details can not be updated.
     */
    void updateBusRoute(BusRoute busRoute) throws DbException;

    /**
     * <p>
     * Inserts new trip to the existing bus in a route with the trip details.
     * 
     * @param busRoute
     *            Object of the persistent class {@link BusRoute} with the trip
     *            details to be persisted
     * @throws DbException
     *             if {@link BusRoute} cannot be inserted.
     */
    void insertTrip(BusRoute busRoute) throws DbException;
}
