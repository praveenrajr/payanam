package com.ideas2it.payanam.dao;

import java.util.Date;
import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;

/**
 * The RouteDAO exposes all the CRUD operations related to the {@link Route}
 * 
 * @author Rajasekar created on 30/08/2017
 */
public interface RouteDAO {

    /**
     * <p>
     * Retrieves all the trips available for the given route and constraints.
     * 
     * @param route
     *            Travel route of the bus which travels from source location to
     *            destination.
     * @param tripDate
     *            Date of the travel.
     * @return busRoutes list of trips which are operated in the particular
     *         route for the given date and constraints
     * @throws DbException
     *             if routes cannot be retrieved.
     * @throws PayanamException
     *             if no route found for the given trip details.
     */
    List<BusRoute> search(Route route, Date tripDate)
            throws DbException, PayanamException;

    /**
     * <p>
     * Retrieves route from the route record matching the from and to location
     * constraints.
     * 
     * @param from
     *            object of the persistent class {@link Location} denoting the
     *            start point of the route.
     * @param to
     *            object of the persistent class {@link Location} denoting the
     *            destination of the route.
     * @return {@link Route} matching the given source and destination
     *         constraints.
     * @throws DbException
     *             if route cannot be retrieved.
     */
    Route getRouteByFromTo(Location from, Location to) throws DbException;
}
