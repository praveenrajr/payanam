package com.ideas2it.payanam.dao;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Location;

/**
 * The BusTypeDAO exposes all the CRUD operations related to the {@link BusType}
 * 
 * @author Rajasekar created on 04/09/2017
 */
public interface BusTypeDAO {

    /**
     * Retrieves all Bus types from the bus type record.
     * 
     * @return locations list of all locations available where buses operate.
     * @throws DbException
     *             if locations cannot be retrieved
     */
    List<BusType> getAllBusTypes() throws DbException;
}
