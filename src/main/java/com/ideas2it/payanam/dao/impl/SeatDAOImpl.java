package com.ideas2it.payanam.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.SeatDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Seat;

/**
 * It interacts with record to retrieve seat details from record.
 * 
 * @author Sachidhanandhan.S created on 06-09-2017
 */
@Repository("seatDao")
public class SeatDAOImpl implements SeatDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.SeatDAO#getSeatById(int)
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public Seat getSeatById(int seatId) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Seat.class);
            criteria.add(Restrictions.eq("id", seatId));
            return (Seat) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(
                    "Exception occured while fetching seat id" + seatId, e);
        }
    }
}