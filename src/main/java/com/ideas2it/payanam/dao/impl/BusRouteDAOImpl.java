package com.ideas2it.payanam.dao.impl;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusRouteDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.BusRoute;

@Repository(Constants.BEAN_BUS_ROUTE_DAO)
/**
 * BusRouteDAOImpl is the implementation of the BusRouteDAO it implements all
 * the methods of the interface.
 */
public class BusRouteDAOImpl implements BusRouteDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusRouteDAO#getBusRouteById(int)
     */
    @Override
    public BusRoute getBusRouteById(int id) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            BusRoute busRoute = session.get(BusRoute.class, id);
            Hibernate.initialize(busRoute.getBus().getSeats());
            return busRoute;
        } catch (HibernateException e) {
            throw new DbException("Exception while retrieving trip by id" + id,
                    e);
        }
    }

    public void updateBusRoute(BusRoute busRoute) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(busRoute);
            transaction.commit();
        } catch (Exception e) {
            throw new DbException(
                    "Exception occurred while updating bus route");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.BusRouteDAO#insertTrip(com.ideas2it.payanam.
     * model.BusRoute)
     */
    @Override
    public void insertTrip(BusRoute busRoute) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(busRoute);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException("Exception while inserting trip", e);
        }
    }
}
