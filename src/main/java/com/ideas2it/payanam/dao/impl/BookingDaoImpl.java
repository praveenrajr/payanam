package com.ideas2it.payanam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BookingDao;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.User;

/**
 * It interacts with record to add, update, retrieve booking details from
 * record.
 * 
 * @author Sachidhanandhan.S created on 04-09-2017
 */
@Repository(Constants.BOOKING_DAO)
public class BookingDaoImpl implements BookingDao {

    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BookingDao#addBookingDetails(com.ideas2it.
     * payanam.model.Booking)
     */
    @Override
    public void addBookingDetails(Booking booking) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.flush();
            session.save(booking);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException("Booking details can not be added", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BookingDao#retrieveBookingDetails()
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public List<Booking> retrieveUserTrips(User user) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Booking.class);
            criteria.add(Restrictions.eq("user", user));
            return (List<Booking>) criteria.list();
        } catch (HibernateException e) {
            throw new DbException(
                    "Execption occured while fetching all booking details of a user"
                            + user.getEmailId(),
                    e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BookingDao#getBooking(int,
     * java.lang.String)
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public Booking retrieveBooking(String ticketId) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Booking.class);
            criteria.add(Restrictions.eq("ticketId", ticketId));
            return (Booking) criteria.uniqueResult();
        } catch (Exception e) {
            throw new DbException(
                    "Exception occured while fetch booking detail" + ticketId,
                    e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.BookingDao#updateBooking(com.ideas2it.payanam.
     * model.Booking)
     */
    @Override
    public void updateBooking(Booking booking) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(booking);
            transaction.commit();
        } catch (Exception e) {
            throw new DbException(
                    "Exception occured while update booking details"
                            + booking.getTicketId(),
                    e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BookingDao#retrieveAllBookingDetails()
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public List<Booking> retrieveAllBookingDetails() throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria critertia = session.createCriteria(Booking.class);
            return (List<Booking>) critertia.list();
        } catch (HibernateException e) {
            throw new DbException(
                    "Execption occured while fetching all booking details", e);
        }
    }
}
