package com.ideas2it.payanam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.Status;

@Repository(Constants.BEAN_BUS_DAO)
/**
 * BusDAOImpl is the implementation of the BusDAO it implements all the methods
 * of the interface.
 */
public class BusDAOImpl implements BusDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusDAO#retrieveAllBuses()
     */
    @Override
    public List<Bus> retrieveAllBuses() throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Bus.class);
            return (List<Bus>) criteria.list();
        } catch (HibernateException e) {
            throw new DbException("Exception while retrieving all buses", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusDAO#retrieveAllAvailableBuses()
     */
    @Override
    public List<Bus> retrieveAllAvailableBuses(Status status)
            throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Bus.class);
            criteria.add(Restrictions.eq("status", status));
            return (List<Bus>) criteria.list();
        } catch (HibernateException e) {
            throw new DbException("Exception while retrieving all buses", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.BusDAO#addBus(com.ideas2it.payanam.model.Bus)
     */
    @Override
    public void addBus(Bus bus) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(bus);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException("Exception while inserting bus with bus regno"
                    + bus.getRegNo(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusDAO#getBusByRegNo(java.lang.String)
     */
    @Override
    public Bus getBusByRegNo(String regNo) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Bus.class);
            criteria.add(Restrictions.eq(Constants.REG_NO, regNo));
            Bus bus = (Bus) criteria.uniqueResult();
            return bus;
        } catch (HibernateException e) {
            throw new DbException(
                    "Exception while retriving bus with the regno " + regNo, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusDAO#getBusById(int)
     */
    @Override
    public Bus getBusById(int id) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Bus.class);
            criteria.add(Restrictions.eq(Constants.ID, id));
            criteria.add(Restrictions.eq(Constants.STATUS, new Status(1)));
            return (Bus) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(
                    "Exception while retriving bus with the Id" + id, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.BusDAO#updateBus(com.ideas2it.payanam.model.Bus)
     */
    @Override
    public void updateBus(Bus bus) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(bus);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException(
                    "Exception while update bus with bus id" + bus.getId(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.BusDAO#deleteBus(com.ideas2it.payanam.model.Bus)
     */
    @Override
    public void deleteBus(Bus bus) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(bus);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException(
                    "Exception while deleting bus with bus id" + bus.getId(),
                    e);
        }
    }
}
