package com.ideas2it.payanam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.BusTypeDAO;
import com.ideas2it.payanam.dao.LocationDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Location;

@Repository(Constants.BEAN_BUS_TYPE_DAO)
/**
 * BusTypeDAOImpl is the implementation of the BusTypeDAO it implements all the
 * methods of the interface.
 */
public class BusTypeDAOImpl implements BusTypeDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.BusTypeDAO#getAllBusTypes()
     */
    @Override
    public List<BusType> getAllBusTypes() throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(BusType.class);
            return (List<BusType>) criteria.list();
        } catch (HibernateException e) {
            throw new DbException(Constants.EXCEPTION_LOCATION_READ);
        }
    }
}
