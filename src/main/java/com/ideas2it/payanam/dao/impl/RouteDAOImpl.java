package com.ideas2it.payanam.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.RouteDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;

@Repository(Constants.BEAN_ROUTE_DAO)
/**
 * RouteDAOImpl is the implementation of the RouteDAO it implements all the
 * methods of the interface.
 */
public class RouteDAOImpl implements RouteDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.RouteDAO#search(
     * com.ideas2it.payanam.model.Route, java.util.Date)
     */
    @Override
    public List<BusRoute> search(Route route, Date tripDate)
            throws DbException, PayanamException {
        try (Session session = sessionFactory.openSession()) {
            Filter filter = session.enableFilter("dateFilter");
            filter.setParameter("filterParam", tripDate);
            Criteria criteria = session.createCriteria(Route.class);
            criteria.add(Restrictions.eq("from", route.getFrom()));
            criteria.add(Restrictions.eq("to", route.getTo()));
            Route retrievedRoute = (Route) criteria.uniqueResult();
            if (null == retrievedRoute) {
                throw new PayanamException(Constants.EXCEPTION_ROUTE_READ
                        + route.getFrom() + route.getTo() + tripDate);
            }
            return new ArrayList<BusRoute>(retrievedRoute.getBusRoutes());
        } catch (HibernateException e) {
            throw new DbException(e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.RouteDAO#getRouteByFromTo(com.ideas2it.payanam.
     * model.Location, com.ideas2it.payanam.model.Location)
     */
    @Override
    public Route getRouteByFromTo(Location from, Location to)
            throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Route.class);
            criteria.add(Restrictions.eq("from", from));
            criteria.add(Restrictions.eq("to", to));
            return (Route) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(e);
        }
    }
}
