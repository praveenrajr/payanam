package com.ideas2it.payanam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.LocationDAO;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Location;

@Repository(Constants.BEAN_LOCATION_DAO)
/**
 * LocationDAOImpl is the implementation of the LocationDAO it implements all
 * the methods of the interface.
 */
public class LocationDAOImpl implements LocationDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.LocationDAO#getAllLocations()
     */
    @Override
    public List<Location> getAllLocations() throws DbException {
        List<Location> locations = null;
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(Location.class);
            locations = criteria.list();
        } catch (HibernateException e) {
            throw new DbException(Constants.EXCEPTION_LOCATION_READ);
        }
        return locations;
    }
}
