package com.ideas2it.payanam.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.dao.UserDao;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.User;

@Repository(Constants.USER_DAO)
/**
 * BusRouteDAOImpl is the implementation of the BusRouteDAO it implements all
 * the methods of the interface.
 *
 * @author Sachidhanandhan.S created on 29-08-2017
 *
 */
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.UserDao#saveUser(com.ideas2it.payanam.model.
     * User)
     */
    @Override
    public boolean saveUser(User user) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
            return true;
        } catch (HibernateException e) {
            throw new DbException("User details can not be saved", e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.UserDao#getUserByEmailId(java.lang.String)
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public User getUserByEmailId(String emailId) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(Constants.EMAILID, emailId));
            return (User) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(
                    "User can not be fetched using email Id" + emailId, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.UserDao#getUserByPhoneNo(java.lang.String)
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public User getUserByPhoneNo(String phoneNo) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq(Constants.PHONENO, phoneNo));
            return (User) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException(
                    "User can not be fetched using phone number" + phoneNo, e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.ideas2it.payanam.dao.UserDao#updateUser(com.ideas2it.payanam.model.
     * User)
     */
    @Override
    public void updateUser(User user) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        } catch (HibernateException e) {
            throw new DbException(
                    "User details can not be updated" + user.getEmailId(), e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.UserDao#getUserByValue(java.lang.String)
     */
    @Override
    @SuppressWarnings(Constants.DEPRECATION)
    public User getUserByValue(String value) throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.disjunction()
                    .add(Restrictions.eq(Constants.PHONENO, value))
                    .add(Restrictions.eq(Constants.EMAILID, value)));
            return (User) criteria.uniqueResult();
        } catch (HibernateException e) {
            throw new DbException("User can not be fetched using value" + value,
                    e);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.ideas2it.payanam.dao.UserDao#retrieveAllUsers()
     */
    @Override
    public List<User> retrieveAllUsers() throws DbException {
        try (Session session = sessionFactory.openSession()) {
            Criteria criteria = session.createCriteria(User.class);
            return (List<User>) criteria.list();
        } catch (HibernateException e) {
            throw new DbException("Exception occurred while fetching all users",
                    e);
        }
    }
}
