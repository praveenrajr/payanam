package com.ideas2it.payanam.dao;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Location;

/**
 * The LocationDAO exposes all the CRUD operations related to the
 * {@link Location}
 * 
 * @author Rajasekar created on 30/08/2017
 */
public interface LocationDAO {

    /**
     * Retrieves all locations from the location record.
     * 
     * @return locations list of all locations available where buses operate.
     * @throws DbException
     *             if locations cannot be retrieved
     */
    List<Location> getAllLocations() throws DbException;
}
