package com.ideas2it.payanam.dao;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.User;

/**
 * It exposes save, search, update operations related to {@link User}
 * 
 * @author Sachidhananadhan.S created on 29-08-2017
 */
public interface UserDao {

    /**
     * It saves users details in user record.
     * 
     * @param user
     *            - user details as object.
     * @return {@link Boolean} true is successfully saved else false.
     * @throws DbException
     *             if user details can not be saved in record.
     */
    boolean saveUser(User user) throws DbException;

    /**
     * It gets user by email id if available in record.
     * 
     * @param emailId
     *            - email id of the user to get his details.
     * @return user object if available in record else null.
     * @throws DbException
     *             if user can not fetched using email id from record.
     */
    User getUserByEmailId(String emailId) throws DbException;

    /**
     * It gets user object by phoneNo if available in record.
     * 
     * @param phoneNo
     *            - phoneNo of the user to get his details.
     * @return user object if available in record else null.
     * @throws DbException
     *             if user details can not be fetched from record using phone
     *             number.
     */
    User getUserByPhoneNo(String phoneNo) throws DbException;

    /**
     * It updates existing user details.
     * 
     * @param user
     *            - new user details to be updated.
     * @throws DbException
     *             if user details can not be updated.
     */
    void updateUser(User user) throws DbException;

    /**
     * It gets user object by value.If available in record it return user object
     * else return null.
     * 
     * @param value
     *            - it can be phone number or email id to login.
     * @return user object if available in record else return null.
     * @throws DbException
     *             If user can not be fetched from record using value.
     */
    User getUserByValue(String value) throws DbException;

    /**
     * It gets all user available in the record
     * 
     * @return list of user objects
     * @throws DbException
     *             If all user can not be fetched from the record.
     */
    List<User> retrieveAllUsers() throws DbException;
}
