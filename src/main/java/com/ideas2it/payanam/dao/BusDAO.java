package com.ideas2it.payanam.dao;

import java.util.List;

import org.springframework.dao.CannotAcquireLockException;
import org.springframework.jca.cci.core.RecordCreator;
import org.springframework.stereotype.Repository;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.Status;

@Repository
/**
 * The BusDAO exposes all the CRUD operations related to the route.
 * 
 * @author Rajasekar created on 04/09/2017
 */
public interface BusDAO {

    /**
     * Retrieves all buses from the bus record which are in active and inactive
     * state.
     * 
     * @return buses list of all buses in the bus record.
     * @throws DbException
     *             if buses cannot be retrieved.
     */
    List<Bus> retrieveAllBuses() throws DbException;

    /**
     * Retrieves all buses from the bus record which are currently in active
     * state.
     * 
     * @param status
     *            object of the persistent model class {@link Status}
     * @return buses list of all available undeleted buses form the bus record.
     * @throws DbException
     *             if buses cannot be retrieved.
     */
    List<Bus> retrieveAllAvailableBuses(Status status) throws DbException;

    /**
     * Inserts a new a bus into the bus record.
     * 
     * @param bus
     *            object of the persistent model class {@link Bus}
     * @throws DbException
     *             if bus cannot inserted successfully.
     */
    void addBus(Bus bus) throws DbException;

    /**
     * Retrieves bus from the bus record identified by the given registration no
     * which is unique for every bus.
     * 
     * @param regNo
     *            Registration no of the bus which is unique for bus in the
     *            record.
     * @return Bus identified with the given registration no.
     * @throws DbException
     *             if Bus cannot be retrieved from the record.
     */
    Bus getBusByRegNo(String regNo) throws DbException;

    /**
     * Retrieves bus from the bus record identified by the given id of the bus
     * which is unique to every bus.
     * 
     * @param id
     *            the identification no of the bus which is unique for every bus
     *            in the bus record
     * @return {@link Bus} object of the persistent class {@link Bus}
     * @throws DbException
     *             if bus cannot be retrieved from the bus record
     * 
     */
    Bus getBusById(int id) throws DbException;

    /**
     * Updates the bus in the bus record with newly given bus data.
     * 
     * @param bus
     *            object of the persistent classs {@link Bus}
     * @throws DbException
     *             if bus cannot be updated in the bus record.
     */
    void updateBus(Bus bus) throws DbException;

    /**
     * Deletes the bus from the bus record identified by the bus id which is
     * unique for every bus in the record.
     * 
     * @param bus
     *            object of the persistent classs {@link Bus}
     * @throws DbException
     *             if bus cannot be deleted from the bus record.
     */
    void deleteBus(Bus bus) throws DbException;
}