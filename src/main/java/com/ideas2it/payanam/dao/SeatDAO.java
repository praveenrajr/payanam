package com.ideas2it.payanam.dao;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Seat;

/**
 * It exposes fetch operations related to seat.
 * 
 * @author Sachidhananadhan.S created on 06-09-2017
 */
public interface SeatDAO {

    /**
     * Retrieves seat for the given seat id.
     * 
     * @param seatId
     *            id of the seat which is unique.
     * @return seat for the given seat no and trip id.
     * @throws DbException
     *             if seat can not be fetched from record
     */
    Seat getSeatById(int seatId) throws DbException;
}
