package com.ideas2it.payanam.dao;

import java.util.List;

import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.User;

/**
 * It exposes booking, search the booked tickets and cancel the booked ticket
 * operations related to user.
 * 
 * @author Sachidhanandhan.S created on 04-09-2017
 */
public interface BookingDao {

    /**
     * It adds new booking details for the user in the record.
     * 
     * @param booking
     *            object has user details, passenger details, bus details and
     *            the seat details.
     * @throws DbException
     *             if booking can not be added in the record.
     */
    void addBookingDetails(Booking booking) throws DbException;

    /**
     * It retrieves user trip details from the record.
     * 
     * @param emailId
     *            unique email id of the user to access his details.
     * @return list of booked and available booking details of the user
     *         available in the record.
     * @throws DbException
     *             if user trips can not be fetched from record.
     */
    List<Booking> retrieveUserTrips(User user) throws DbException;

    /**
     * It retrieves booking details using ticket id.If not available in the
     * record it returns null
     * 
     * @param ticketId
     *            unique ticket id for the user to travel
     * @return booking object if available in the record for the unique id.If
     *         not returns null.
     * @throws DbException
     *             if booking details can not be fetched from record.
     */
    Booking retrieveBooking(String ticketId) throws DbException;

    /**
     * It updates booking details existing in the record.
     * 
     * @param booking
     *            new booking details to be updated in the record.
     * @throws DbException
     *             if booking details can not be updated in the record.
     */
    void updateBooking(Booking booking) throws DbException;

    /**
     * It fetches all the available booking details from the record.
     * 
     * @return list of booking details available in the record.
     * @throws DbException
     *             if list of booking details can not be fetched from the
     *             record.
     */
    List<Booking> retrieveAllBookingDetails() throws DbException;
}
