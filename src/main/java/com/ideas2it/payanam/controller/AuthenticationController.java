package com.ideas2it.payanam.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.ideas2it.payanam.common.Constants;

/**
 * It filters the request.It checks whether the session is created for the user
 * who sends the request. If session is not created it redirects to index page.
 * else it allows the request to proceed further.
 *
 * @author S.Sachidhanandhan created on 07/09/2017
 */
public class AuthenticationController extends HandlerInterceptorAdapter {

    /**
     * It checks whether the request from the user has a session or not.If
     * session is not created it redirects to index page else the request is
     * proceed further.
     * 
     * @param request
     *            - It contains request from the user.
     * @param response
     *            - response object for the user.
     * @throws ServletException
     *             - if an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             - if the http request could not be handled.
     */
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
        switch (request.getRequestURI()) {
        case "/Payanam/login":
            break;
        case "/Payanam/registrationForm":
            break;
        case "/Payanam/addUser":
            break;
        default:
            HttpSession session = request.getSession(false);
            if (null != session.getAttribute(Constants.ID)) {
                response.setHeader(Constants.CACHE_CONTROL,
                        Constants.NO_CACHE_NO_STORE);
                response.setHeader(Constants.PRAGMA, Constants.NO_CACHE);
                response.setDateHeader(Constants.EXPIRES, 0);
            } else {
                response.sendRedirect(Constants.REDIRECT);
                return false;
            }
        }
        return true;
    }
}