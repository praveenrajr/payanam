package com.ideas2it.payanam.controller;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.service.BookingService;
import com.ideas2it.payanam.service.UserService;

@Controller
public class AdminController {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private UserService userService;

    /**
     * Gets user details and his booking details with trip history.
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping(Constants.GET_USER_AND_TRIP_DETAILS)
    ModelAndView getUserDetails(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.USER_ADMIN_VIEW);
        try {
            modelAndView.addObject(Constants.USER, userService
                    .getUser(request.getParameter(Constants.EMAILID)));
            Map<String, Set<Booking>> userTrips = bookingService
                    .getUserTrips((String) request.getSession(Boolean.FALSE)
                            .getAttribute(Constants.ID));
            modelAndView.addObject(Constants.UPCOMING_TRIPS,
                    userTrips.get(Constants.UPCOMING_TRIPS));
            modelAndView.addObject(Constants.PAST_TRIPS,
                    userTrips.get(Constants.PAST_TRIPS));
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR, "Something went wrong can"
                    + " not display details please try again later");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }
}
