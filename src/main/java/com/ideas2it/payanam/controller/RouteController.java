package com.ideas2it.payanam.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;
import com.ideas2it.payanam.service.LocationService;
import com.ideas2it.payanam.service.RouteService;

/**
 * The RouteController takes requests controls the logic and handles the user
 * request related to the {@link Route} which is the geological location where
 * buses operate then responds the user with the result for the operation for
 * the requested operation.
 * 
 * @author Rajasekar created on 2017/08/30
 */
@Controller
public class RouteController {
    @Autowired
    private RouteService routeService;

    @Autowired
    private LocationService locationService;

    /**
     * <p>
     * Retrieves all locations where buses operate and redirects the user to the
     * home page.
     * 
     * @return modelAndView Object containing model objects and view page
     *         information to redirect.
     */
    @RequestMapping(Constants.ROUTE_HOME)
    public ModelAndView getLocations() {
        ModelAndView modelAndView = new ModelAndView(Constants.SEARCH);
        try {
            modelAndView.addObject(Constants.LOCATIONS,
                    locationService.getAllLocations());
        } catch (DbException | PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
        }
        return modelAndView;
    }

    /**
     * <p>
     * Searches for the trips available for the given route constraints,
     * retrieves the trip and bus details as a list.
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return modelAndView Object containing model objects and view page
     *         information to redirect.
     */
    @RequestMapping(Constants.ROUTE_SEARCH)
    public ModelAndView search(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BUSES);
        int from = Integer.valueOf(request.getParameter(Constants.FROM));
        int to = Integer.valueOf(request.getParameter(Constants.TO));
        List<Location> locations = null;
        Route route = new Route(new Location(from), new Location(to));
        try {
            locations = locationService.getAllLocations();
            Date tripDate = new SimpleDateFormat(Constants.DATE_FORMAT)
                    .parse(request.getParameter(Constants.TRIP_DATE));
            List<BusRoute> routes = routeService.search(route, tripDate);
            modelAndView.addObject(Constants.LOCATIONS, locations);
            modelAndView.addObject(Constants.BUS_ROUTES, routes);
        } catch (ParseException e) {
            modelAndView = new ModelAndView(Constants.SEARCH);
            modelAndView.addObject(Constants.MESSAGE, Constants.DATE_INVALID);
            modelAndView.addObject(Constants.LOCATIONS, locations);
            PayanamLogger.log(e);
        } catch (PayanamException e) {
            modelAndView = new ModelAndView(Constants.SEARCH);
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.NO_BUSES_OPERATING);
            modelAndView.addObject(Constants.LOCATIONS, locations);
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView = new ModelAndView(Constants.SEARCH);
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.BUS_FETCH_FAILED);
            modelAndView.addObject(Constants.LOCATIONS, locations);
            PayanamLogger.log(e);
        }
        return modelAndView;
    }
}
