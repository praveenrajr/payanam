package com.ideas2it.payanam.controller;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Location;
import com.ideas2it.payanam.model.Route;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.service.BusRouteService;
import com.ideas2it.payanam.service.BusService;
import com.ideas2it.payanam.service.LocationService;
import com.ideas2it.payanam.service.RouteService;

@Controller
/**
 * The BusRouteController takes requests controls the logic and handles the user
 * requests of the BusRoute then responds the user with the result for the
 * operation for the requested operation.
 * 
 * @author Rajasekar created on 2017/09/07
 */
public class BusRouteController {
    @Autowired
    private BusRouteService busRouteService;
    @Autowired
    private BusService busService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private RouteService routeService;

    /**
     * Add trip to the bus identified with the given registration number which
     * is unique for every bus in the record.
     */
    @RequestMapping(Constants.ADD_BUS_TRIP)
    ModelAndView addTrip(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS);
        try {
            Bus bus = busService
                    .getBusByRegNo(request.getParameter(Constants.REG_NO));
            int fromId = Integer.parseInt(request.getParameter(Constants.FROM));
            int toId = Integer.parseInt(request.getParameter(Constants.TO));
            Date date = new SimpleDateFormat(Constants.DATE_FORMAT)
                    .parse(request.getParameter(Constants.TRIP_DATE));
            Date departure = new SimpleDateFormat(Constants.TIME_FORMAT)
                    .parse(request.getParameter(Constants.ARRIVAL_TIME));
            Date arrival = new SimpleDateFormat(Constants.TIME_FORMAT)
                    .parse(request.getParameter(Constants.DEPARTURE_TIME));
            Route route = routeService.getRouteByFromTo(new Location(fromId),
                    new Location(toId));
            int price = Integer.valueOf(request.getParameter("price"));
            BusRoute busRoute = new BusRoute(new Time(arrival.getTime()),
                    new Time(departure.getTime()), date, price, bus, route,
                    new Status(1));
            busRouteService.addBusRoute(busRoute);

            modelAndView.addObject(Constants.BUS, busService
                    .getBusByRegNo(request.getParameter(Constants.REG_NO)));
            modelAndView.addObject(Constants.LOCATIONS,
                    locationService.getAllLocations());
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        } catch (ParseException e) {
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.DATE_INVALID + "or Time");
            PayanamLogger.log(e);
        }
        return modelAndView;
    }
}
