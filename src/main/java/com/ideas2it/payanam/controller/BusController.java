package com.ideas2it.payanam.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebParam.Mode;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scripting.bsh.BshScriptUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Bus;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.BusType;
import com.ideas2it.payanam.model.Route;
import com.ideas2it.payanam.model.Seat;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.model.User;
import com.ideas2it.payanam.service.BusService;
import com.ideas2it.payanam.service.BusTypeService;
import com.ideas2it.payanam.service.LocationService;

/**
 * The BusController takes requests controls the logic and handles the user
 * request related to the {@link Bus} which is the transport medium. then
 * responds the user with the result for the operation for the requested
 * operation of the {@link Bus}.
 * 
 * @author Rajasekar created on 2017/09/05
 */
@Controller
public class BusController {
    @Autowired
    BusService busService;
    @Autowired
    BusTypeService busTypeService;
    @Autowired
    LocationService locationService;

    /**
     * Retrieves all the buses from the bus record including the buses which are
     * not active.
     * 
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping("getBuses")
    public ModelAndView getAllBuses() {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS_DASHBOARD);
        try {
            modelAndView.addObject(Constants.BUS_TYPES, busTypeService.getAllBusTypes());
            System.out.println(busTypeService.getAllBusTypes());
            modelAndView.addObject(Constants.BUSES, busService.getAllBuses());
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.CONNECTION_FAILURE);
            PayanamLogger.log(e);
        }
        return modelAndView;
    }

    /**
     * Retrives all buses from the record which are currently in active state.
     * 
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping("getActiveBuses")
    public ModelAndView getAllActiveBuses() {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS_DASHBOARD);
        try {
            modelAndView.addObject(Constants.BUSES,
                    busService.getAllAvailableBuses());
            modelAndView.addObject(Constants.BUS_TYPES,
                    busTypeService.getAllBusTypes());
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.CONNECTION_FAILURE);
            PayanamLogger.log(e);
        }
        return modelAndView;
    }

    /**
     * Adds a new bus to the bus record after validating as the reliability of
     * the data is important.
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping(Constants.ROUTE_ADD_BUS)
    public ModelAndView addBus(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS_DASHBOARD);
        try {
            int busTypeId = Integer
                    .valueOf(request.getParameter(Constants.BUS_TYPE_ID));
            int noOfSeats = Integer
                    .valueOf(request.getParameter(Constants.NO_OF_SEATS));
            BusType busType = new BusType(busTypeId);
            Bus bus = new Bus(request.getParameter(Constants.REG_NO),
                    request.getParameter(Constants.NAME), noOfSeats,
                    new BusType(busTypeId), new Status(1));
            busService.addBus(bus);
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.CONNECTION_FAILURE);
            PayanamLogger.log(e);
        }
        return modelAndView;
    }

    /**
     * Deletes {@link Bus} from the bus record identified by the bus
     * identification id.
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping(Constants.DELETE_BUS)
    public ModelAndView deleteBus(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS_DASHBOARD);
        try {
            busService.deleteBus(request.getParameter(Constants.REG_NO));
            modelAndView.addObject(Constants.MESSAGE, "Deleted successfully.!");
        } catch (DbException | PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        }
        return modelAndView;
    }

    /**
     * Gets a bus by the unique registration no from the bus record.
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping(Constants.ROUTE_GET_BUS_REGNO)
    public ModelAndView getBusByRegNo(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BUS);
        try {
            modelAndView.addObject(Constants.BUS, busService
                    .getBusByRegNo(request.getParameter(Constants.REG_NO)));
            modelAndView.addObject(Constants.LOCATIONS,
                    locationService.getAllLocations());
        } catch (DbException | PayanamException e) {
            modelAndView.addObject(Constants.ERROR, e.getMessage());
            PayanamLogger.log(e);
        }
        return modelAndView;
    }
}
