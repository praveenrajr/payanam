package com.ideas2it.payanam.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;

/**
 * It logout from the account of the user by closing the session.Once logout
 * user has to login to access his/her account.
 * 
 * @author Sachidhanandhan.S Created on 30-08-2017
 *
 */
@Controller
public class LogoutController {

    /**
     * It takes the session object and invalid it so the the of the current user
     * is cleared.
     * 
     * @param request
     *            - It contains session object which will be needed to close the
     *            session.
     * @return index page.
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.LOGOUT)
    ModelAndView logout(HttpServletRequest request)
            throws ServletException, IOException {
        HttpSession session = request.getSession(Boolean.FALSE);
        session.invalidate();
        return new ModelAndView(Constants.REDIRECT);
    }
}
