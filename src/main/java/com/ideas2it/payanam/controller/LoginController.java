package com.ideas2it.payanam.controller;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.service.UserService;

/**
 * It takes the value from the user to create a session for the particular value
 * .It will create session only if password for the id matches in the record
 * else throws error to the user.
 * 
 * @author Sachidhananadhan.S created on 30-08-2017
 *
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    /**
     * It creates new session if password for the id matches in record else send
     * boolean value true.
     * 
     * @param request
     *            - It contains user values which will be needed to create the
     *            session
     * @return session with id will be sent to user.
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.LOGIN)
    public ModelAndView loginAuthentication(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView;
        try {
            Map<String, Object> status = userService.loginAuthenticate(
                    request.getParameter(Constants.EMAILID),
                    request.getParameter(Constants.PASSWORD));
            if ((Boolean) status.get(Constants.STATUS)) {
                modelAndView = new ModelAndView((Constants.REDIRECT_HOME));
                HttpSession session = request.getSession();
                session.setAttribute(Constants.ID,
                        request.getParameter(Constants.EMAILID));
                session.setAttribute(Constants.ROLE,
                        status.get(Constants.ROLE));

            } else {
                modelAndView = new ModelAndView(Constants.REDIRECT);
                modelAndView.addObject(Constants.ERROR,
                        "Invalid e-mail or password");
            }
        } catch (DbException e) {
            modelAndView = new ModelAndView(Constants.REDIRECT);
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, cannot login");
            PayanamLogger.log(e);
        } catch (InvalidInputException e) {
            modelAndView = new ModelAndView(Constants.REDIRECT);
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, can not login please try again later");
        }
        return modelAndView;
    }
}
