package com.ideas2it.payanam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jws.WebParam.Mode;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.BookingDetails;
import com.ideas2it.payanam.model.BusRoute;
import com.ideas2it.payanam.model.Seat;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.service.BusRouteService;

/**
 * The SeatController takes requests controls the logic and handles the user
 * requests of the seat then responds the user with the result for the operation
 * for the requested operation.
 * 
 * @author Rajasekar created on 2017/09/04
 */
@Controller
public class SeatController {
    @Autowired
    private BusRouteService busRouteService;

    /**
     * <p>
     * Gets all available seats and unavailable seats and their status from seat
     * record for the respective bus route (trip).
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         be passed
     */
    @RequestMapping(Constants.ROUTE_SEATS)
    public ModelAndView getAllSeatsByTrip(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.SEATS);
        int busRouteId = Integer.valueOf(request.getParameter("id"));
        try {
            BusRoute busRoute = busRouteService.getBusRouteById(busRouteId);
            Set<Seat> seats = busRoute.getBus().getSeats();
            Map<Integer, Seat> seatStatus = new HashMap<>();
            for (Seat seat : seats) {
                seatStatus.put(Integer.valueOf(seat.getSeatNo()), seat);
            }
            for (Booking booking : busRoute.getBookings()) {
                for (BookingDetails bookingDetails : booking
                        .getBookingDetails()) {
                    if (!bookingDetails.getStatus().getStatus()
                            .equalsIgnoreCase(Constants.CANCELLED)) {
                        bookingDetails.getSeat()
                                .setStatus(new Status(Constants.BOOKED));
                        seatStatus.put(
                                Integer.valueOf(
                                        bookingDetails.getSeat().getSeatNo()),
                                bookingDetails.getSeat());
                    }
                }
            }
            modelAndView.addObject(Constants.SEATS, seatStatus);
            modelAndView.addObject(Constants.BUS_ROUTE, busRoute);
        } catch (DbException e) {
            modelAndView.addObject(Constants.MESSAGE,
                    Constants.CONNECTION_FAILURE);
            PayanamLogger.log(e);
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.MESSAGE, e.getMessage());
            PayanamLogger.log(e);
        }
        return modelAndView;
    }
}
