package com.ideas2it.payanam.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.model.Role;
import com.ideas2it.payanam.model.User;
import com.ideas2it.payanam.service.UserService;

/**
 * Takes request to add, update user details in the user record. Search user by
 * email id from the record , change password for the user. Displays all user
 * available in the record.
 * 
 * @author Sachidhanandhan.S created on 29-08-2017
 *
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * Takes the request and redirects the user to registration form with the
     * empty {@link User} object
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} Object containing model objects and view
     *         page information to redirect.
     */
    @RequestMapping(Constants.ROUTE_REGISTRATION_FORM)
    public ModelAndView registrationForm(HttpServletRequest request) {
        return new ModelAndView(Constants.REGISTRATION, "User", new User());
    }

    /**
     * It takes user details and add it to the record.
     * 
     * @param request
     *            user object contains details about the user.
     * @param user
     *            object of the persistent class {@link User} which is related
     *            to a user.
     * @return {@link ModelAndView} Object containing model objects and view
     *         page information to redirect.
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.ADD_USER)
    public ModelAndView addUser(@ModelAttribute User user,
            HttpServletRequest request) throws ServletException, IOException {
        ModelAndView modelAndView = null;
        try {
            user.setRole(new Role(1, "user"));
            Map<String, Object> status = userService.addUser(user);
            if ((Boolean) status.get(Constants.STATUS)) {
                modelAndView = new ModelAndView(Constants.REDIRECT_HOME);
                modelAndView.addObject(Constants.ROLE,
                        status.get(Constants.ROLE));
                HttpSession session = request.getSession();
                session.setAttribute(Constants.ID,
                        request.getParameter(Constants.EMAILID));
            }
        } catch (InvalidInputException e) {
            modelAndView = new ModelAndView(Constants.REGISTRATION);
            modelAndView.addObject(Constants.ERROR, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView = new ModelAndView(Constants.REGISTRATION);
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, user can not be registered");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It takes user details to be updated in user record.
     * 
     * @param request
     *            user details to be updated.
     * @return {@link ModelAndView} Object containing model objects and view
     *         page information to redirect.
     */
    @RequestMapping(Constants.UPDATE_USER)
    public ModelAndView updateUser(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.USER);
        User user = null;
        try {
            user = userService.getUser(request.getParameter(Constants.EMAILID));
            modelAndView.addObject(Constants.SUCCESS,
                    userService.updateUser(
                            request.getParameter(Constants.PHONENO),
                            request.getParameter(Constants.EMAILID),
                            request.getParameter(Constants.DOB),
                            request.getParameter(Constants.FIRSTNAME),
                            request.getParameter(Constants.LASTNAME)));
            modelAndView.addObject(Constants.USER, userService
                    .getUser(request.getParameter(Constants.EMAILID)));
        } catch (DbException e) {
            modelAndView.addObject(Constants.USER, user);
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, can not update");
            PayanamLogger.log(e);
        } catch (InvalidInputException e) {
            modelAndView.addObject(Constants.USER, user);
            modelAndView.addObject(Constants.ERROR, e.getMessage());
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It gets user details from record using email Id.
     * 
     * @param request
     *            unique email id of the User to get his details
     * @return User object if available else sends error message
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.GET_USER)
    public ModelAndView getUser(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constants.USER);
        try {
            modelAndView.addObject(Constants.USER,
                    userService
                            .getUser((String) request.getSession(Boolean.FALSE)
                                    .getAttribute(Constants.ID)));
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR, "Something went wrong can"
                    + " not display details please try again later");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It gets all user from record.
     * 
     * @param request
     *            request from client to get all user from record
     * @return list of users as user object in modelAndView.
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.GET_ALL_USER)
    public ModelAndView getAllUser(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constants.USER_DASHBOARD);
        try {
            modelAndView.addObject(Constants.USERS, userService.getAllUsers());
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, can not fetch all users");
        } finally {
            return modelAndView;
        }
    }

    /**
     * It changes the password if the old password matches the password of the
     * user in record.
     * 
     * @param request
     *            unique email id to access his/her account, old password to
     *            login into his/her account and new password to change the
     *            existing password.
     * @return success message if password updated
     * @throws ServletException
     *             If an input or output error is detected when the servlet
     *             handles the request
     * @throws IOException
     *             If the request could not be handled
     */
    @RequestMapping(Constants.CHANGEPASSWORD)
    public ModelAndView changePassword(HttpServletRequest request)
            throws ServletException, IOException {
        ModelAndView modelAndView = new ModelAndView(Constants.CHANGEPASSWORD);
        try {
            userService.changePassword(request.getParameter(Constants.EMAILID),
                    request.getParameter(Constants.OLD_PASSWORD),
                    request.getParameter(Constants.NEW_PASSWORD));
            HttpSession session = request.getSession(Boolean.FALSE);
            session.invalidate();
            modelAndView = new ModelAndView(Constants.REDIRECT);
            modelAndView.addObject(Constants.SUCCESS,
                    Constants.SUCCESS_PASSWORD);
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, can not change password");
        } catch (InvalidInputException e) {
            modelAndView.addObject(Constants.ERROR, e.getMessage());
        } finally {
            return modelAndView;
        }
    }
}
