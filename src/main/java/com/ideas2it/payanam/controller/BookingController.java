package com.ideas2it.payanam.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ideas2it.payanam.common.Constants;
import com.ideas2it.payanam.common.PayanamLogger;
import com.ideas2it.payanam.exception.DbException;
import com.ideas2it.payanam.exception.InvalidInputException;
import com.ideas2it.payanam.exception.PayanamException;
import com.ideas2it.payanam.model.Booking;
import com.ideas2it.payanam.model.BookingDetails;
import com.ideas2it.payanam.model.Seat;
import com.ideas2it.payanam.model.Status;
import com.ideas2it.payanam.service.BookingService;
import com.ideas2it.payanam.service.SeatService;

/**
 * The SeatController takes requests, controls the logic and handles the user
 * request then responds the user with the result for the operation for the
 * requested operation.
 * 
 * @author Sachidhanandhan created on 2017/09/04
 */
@Controller
public class BookingController {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private SeatService seatService;

    /**
     * Takes the request and redirects the user to the passenger form where no
     * of passenger details can be filled for booking
     * 
     * @param request
     *            the HttpServletRequest object that contains the request the
     *            client made of the servlet
     * @return {@link ModelAndView} respective view's identifier and objects to
     *         to sent as response
     */
    @RequestMapping(Constants.PASSENGERS_FORM)
    public ModelAndView showPassengerForms(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.PASSENGERS);
        String[] selectedSeats = request.getParameterValues("seats");
        modelAndView.addObject(Constants.NO_OF_PASSENGERS,
                selectedSeats.length);
        modelAndView.addObject(Constants.SELECTED_SEATS, selectedSeats);
        modelAndView.addObject(Constants.BUS_ROUTE_ID,
                request.getParameter(Constants.BUS_ROUTE_ID));
        return modelAndView;
    }

    /**
     * Takes request to book a ticket for the user.It has booking details to be
     * booked for the user.It also books list of seat for maximum of six
     * passengers
     * 
     * @param request
     *            Booking details of the user to be saved in the record.
     * @return ticket id, booking details and the success message.
     */
    @RequestMapping(Constants.ADD_BOOKING)
    public ModelAndView addBooking(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BOOKING_DETAILS);
        try {
            Set<BookingDetails> bookingDetails = new HashSet<>();
            BookingDetails bookingDetail = new BookingDetails();
            int noOfPassenger = Integer
                    .parseInt(request.getParameter(Constants.NO_OF_PASSENGER));
            for (int minPassenger = 1; minPassenger < noOfPassenger; minPassenger++) {
                Seat seat = seatService.getSeatById(Integer.parseInt(
                        request.getParameter(Constants.SEAT + minPassenger)));
                bookingDetail = new BookingDetails(
                        request.getParameter(Constants.NAME + minPassenger),
                        request.getParameter(Constants.GENDER + minPassenger),
                        Integer.parseInt(request
                                .getParameter(Constants.AGE + minPassenger)),
                        seat, new Status(3));
                bookingDetails.add(bookingDetail);
            }
            Map<String, String> booking = bookingService.addBookingDetails(
                    Integer.parseInt(
                            request.getParameter(Constants.BUS_ROUTE_ID)),
                    request.getParameter(Constants.EMAILID), bookingDetails,
                    noOfPassenger);
            modelAndView.addObject(Constants.BOOKING,
                    bookingService.getBookedTicket(
                            booking.get(Constants.TICKET_ID),
                            request.getParameter(Constants.EMAILID)));

            modelAndView.addObject(Constants.SUCCESS,
                    booking.get(Constants.STATUS));
        } catch (PayanamException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Bus Route is not available");
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Somthing went wrong, can not book bus");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It takes request from the user to get the booked ticket from the record.
     * 
     * @param request
     *            ticket id and email id of the user to get booking details
     * @return booked details available in the record.
     */
    @RequestMapping(Constants.GET_BOOKED_DETAIL)
    public ModelAndView getBookedDetail(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.BOOKING_DETAILS);
        try {
            modelAndView.addObject(Constants.BOOKING,
                    bookingService.getBookedTicket(
                            request.getParameter(Constants.TICKET_ID),
                            request.getParameter(Constants.EMAILID)));
        } catch (InvalidInputException e) {
            modelAndView.addObject(Constants.ERROR, e.getMessage());
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Somthing went wrong could not get booking details");
        } finally {
            return modelAndView;
        }
    }

    /**
     * It takes request from the user to get all his/her trip details available
     * in the record.
     * 
     * @param request
     *            request from the user to get all his trip details
     * @return upcoming trip details and the past trip details.
     */
    @RequestMapping(Constants.MY_TRIPS)
    public ModelAndView getUserTrips(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.MY_TRIPS);
        try {
            Map<String, Set<Booking>> userTrips = bookingService
                    .getUserTrips((String) request.getSession(Boolean.FALSE)
                            .getAttribute(Constants.ID));
            modelAndView.addObject(Constants.UPCOMING_TRIPS,
                    userTrips.get(Constants.UPCOMING_TRIPS));
            modelAndView.addObject(Constants.PAST_TRIPS,
                    userTrips.get(Constants.PAST_TRIPS));
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Somthing went wrong, can not display booking details");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It takes request to display all booking details available in the record.
     * 
     * @param request
     *            requested by the admin to get all booking details.
     * @return list of booking details else error message
     */
    @RequestMapping(Constants.GET_ALL_BOOKING_DETAILS)
    public ModelAndView getAllBookingDetails(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.addObject(Constants.ALL_BOOKING_DETAILS,
                    bookingService.getAllBookingDetails());
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong can not display all booking details");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }

    /**
     * It takes request to cancel the booked ticket and returns success or error
     * message depending upon the process.
     * 
     * @param request
     *            ticket id, email id and booking details of the user to cancel
     *            the ticket.
     * @return error message if email id or ticket id is invalid.
     */
    @RequestMapping(Constants.TICKET_CANCEL)
    public ModelAndView cancelTicket(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView(Constants.MY_TRIPS);
        try {
            modelAndView.addObject(Constants.SUCCESS,
                    bookingService.cancelTicket(
                            request.getParameter(Constants.TICKET_ID),
                            request.getParameter(Constants.EMAILID),
                            request.getParameter(Constants.PASSENGER_ID)));
            Map<String, Set<Booking>> userTrips = bookingService
                    .getUserTrips((String) request.getSession(Boolean.FALSE)
                            .getAttribute(Constants.ID));
            modelAndView.addObject(Constants.UPCOMING_TRIPS,
                    userTrips.get(Constants.UPCOMING_TRIPS));
            modelAndView.addObject(Constants.PAST_TRIPS,
                    userTrips.get(Constants.PAST_TRIPS));
        } catch (InvalidInputException e) {
            modelAndView.addObject(Constants.ERROR, e.getMessage());
            PayanamLogger.log(e);
        } catch (DbException e) {
            modelAndView.addObject(Constants.ERROR,
                    "Something went wrong, can not cancel ticket please try again later");
            PayanamLogger.log(e);
        } finally {
            return modelAndView;
        }
    }
}
