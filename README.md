# Payanam

Payanam is online bus ticket booking web application. 

### Tech

Payanam uses a number of open source projects to work properly:

* [Spring Web MVC] - Framework for MVC projects
* [Hibernate] - Hibernate ORM helps application to achieve persistence
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* Java .
* Apache Tomcat server.

### Installation

Payanam requires [Java - 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html)  to run.

                              JAVA INSTALLATION 
Installing Java with apt-get is easy. First, update the package index:
1)sudo apt-get update
Then, check if Java is not already installed:
2)java -version.
If it returns "The program java can be found in the following packages", Java hasn't been installed yet, so execute the following command:
3)sudo apt-get install default-jre
Install the dependencies and devDependencies and start the server.

This will install the Java Runtime Environment (JRE). If you instead need the Java Development Kit (JDK), which is usually needed to compile Java applications (for example Apache Ant, Apache Maven, Eclipse and IntelliJ IDEA execute the following command:
4)sudo apt-get install default-jdk.
5)Installing Oracle JDK (optional)
The Oracle JDK is the official JDK; however, it is no longer provided by Oracle as a default installation for Ubuntu.
You can still install it using apt-get. To install any version, first execute the following commands:

sudo apt-get install python-software-properties
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update.

6)Oracle JDK 8

This is a developer preview, the general release is scheduled for March 2014. This external article about Java 8 may help you to understand what it's all about.
sudo apt-get install oracle-java8-installer

                              Apache Maven 

  What is it?
  -----------

  Maven is a software project management and comprehension tool. Based on
  the concept of a Project Object Model (POM), Maven can manage a project's
  build, reporting and documentation from a central piece of information.

  Documentation
  -------------

  The most up-to-date documentation can be found at https://maven.apache.org/.

  Release Notes
  -------------

  The full list of changes can be found at https://maven.apache.org/docs/history.html.

  System Requirements
  -------------------

  JDK:
    1.7 or above (this is to execute Maven - it still allows you to build against 1.3
    and prior JDK's).
  Memory:
    No minimum requirement.
  Disk:
    Approximately 10MB is required for the Maven installation itself. In addition to
    that, additional disk space will be used for your local Maven repository. The size
    of your local repository will vary depending on usage but expect at least 500MB.
  Operating System:
    Windows:
      Windows 2000 or above.
    Unix based systems (Linux, Solaris and Mac OS X) and others:
      No minimum requirement.

  Installing Maven
  ----------------

  1) Unpack the archive where you would like to store the binaries, e.g.:

    Unix-based operating systems (Linux, Solaris and Mac OS X)
      tar zxvf apache-maven-3.x.y.tar.gz
    Windows
      unzip apache-maven-3.x.y.zip

  2) A directory called "apache-maven-3.x.y" will be created.

  3) Add the bin directory to your PATH, e.g.:

    Unix-based operating systems (Linux, Solaris and Mac OS X)
      export PATH=/usr/local/apache-maven-3.x.y/bin:$PATH
    Windows
      set PATH="c:\program files\apache-maven-3.x.y\bin";%PATH%

  4) Make sure JAVA_HOME is set to the location of your JDK

  5) Run "mvn --version" to verify that it is correctly installed.

  Maven URLS
  ----------

  Home Page:          https://maven.apache.org/
  Downloads:          https://maven.apache.org/download.html
  Release Notes:      https://maven.apache.org/docs/history.html
  Mailing Lists:      https://maven.apache.org/mail-lists.html
  Source Code:        https://git-wip-us.apache.org/repos/asf/maven.git
  Issue Tracking:     https://issues.apache.org/jira/browse/MNG
  Wiki:               https://cwiki.apache.org/confluence/display/MAVEN/
  Available Plugins:  https://maven.apache.org/plugins/index.html


